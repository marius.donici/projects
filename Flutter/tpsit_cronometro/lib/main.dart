import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(Cronometro());
}

class Cronometro extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              backgroundColor: Color.fromRGBO(98, 82, 97, 2),
              title: Center(
                  child: Text(
                'Cronometro',
                textAlign: TextAlign.center,
                style: TextStyle(color: Color.fromRGBO(232, 232, 232, 1)),
              )),
            ),
            body: CronometroFul()));
  }
}

class CronometroFul extends StatefulWidget {
  _CronometroFulState createState() => _CronometroFulState();
}

class _CronometroFulState extends State<CronometroFul> {
  int seconds = 0;
  int minutes = 0;
  StreamSubscription sub;

  bool playIsPressed = false;

  _CronometroFulState() {
    sub = ticker().listen((event) {
      increment();
    });
    sub.pause();
  }

  void increment() {
    setState(() {
      if (seconds == 59) {
        seconds = 0;
        minutes++;
      } else {
        seconds++;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return (Container(
        color: Color.fromRGBO(232, 232, 232, 1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Text(
                    "$minutes",
                    style: TextStyle(
                      color: Color.fromRGBO(98, 82, 97, 1),
                      fontSize: 100,
                    ),
                  ),
                ),
                SizedBox(
                  child: Text(
                    ":",
                    style: TextStyle(
                        fontSize: 100, color: Color.fromRGBO(98, 82, 97, 1)),
                  ),
                  width: 20,
                ),
                Container(
                  child: Text(
                    "$seconds",
                    style: TextStyle(
                      color: Color.fromRGBO(98, 82, 97, 1),
                      fontSize: 100,
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                    child: IconButton(
                  color: Color.fromRGBO(137, 190, 179, 1),
                  iconSize: 75,
                  onPressed: () {
                    if (playIsPressed) {
                      setState(() {
                        playIsPressed = false;
                        sub.pause();
                      });
                    } else {
                      setState(() {
                        playIsPressed = true;
                        sub.resume();
                      });
                    }
                  },
                  icon: playIsPressed
                      ? new Icon(Icons.pause)
                      : new Icon(Icons.play_arrow),
                )),
                Container(
                  child: IconButton(
                    iconSize: 75,
                    color: Color.fromRGBO(137, 190, 179, 1),
                    icon: Icon(Icons.delete),
                    onPressed: () => deleteButton(),
                  ),
                )
              ],
            ),
          ],
        )));
  }

  void deleteButton() {
    setState(() {
      if (!sub.isPaused) {
        sub.pause();
      }
      minutes = 0;
      seconds = 0;
      playIsPressed = false;
    });
  }
}

Stream<bool> ticker() async* {
  while (true) {
    await Future.delayed(Duration(seconds: 1));
    yield true;
  }
}
