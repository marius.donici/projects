Si progetti una app per note (memo) con le seguenti caratteristiche:
- ogni memo ha un titolo ed un corpo in formato md
- ogni memo ha un account Google di riferimento che ne indica il creatore (email Google)
- ogni memo appartiene ad una categoria (in caso si definisca una categoria di default)
- per un memo ci possono essere più TAG, ad ogni TAG corrisponde almeno un memo, i TAG sono creati eventualmente quando si crea un memo (non è un'operazione separata)
- un memo può essere condiviso con altri utenti Google

Per prima cosa pensiamo alla creazione del database, iniziando con l’individuare le entità e gli attributi.

MEMO : id, titolo, corpo
ACCOUNT : id, mail, password
CATEGORIA : id, categoria
TAG : id, tag
 
Dopo aver individuato le entità, passiamo a capire come sono relazionarle tra di loro. 
Tra MEMO e ACCOUNT creiamo una relazione di CREAZIONE di molti a uno, cioè un memo può essere creato solamente da un account, invece un account può creare più memo.
Inoltre tra MEMO e ACCOUNT c’è una relazione di CONDIVISIONE molti a molti, infatti un utente può condividere più memo, e un memo può essere condiviso da più utenti.
Tra MEMO e CATEGORIA associamo una relazione di APPARTENERE molti a uno, un memo può appartenere solamente ad una categoria, ma una categoria può includere più memo.
Tra MEMO e TAG c’è una relazione di CONTENERE molti a molti, un memo può contenere più tag e più tag possono essere contenuti da più memo.

Dopo aver installato le dependencies di cui si ha bisogno, passiamo a creare i modelli in Dart delle entità e ed i loro DAO. Più precisamente abbiamo i seguenti modelli e le loro rispettive dao:
Memo, Category, Tag, TaggedMemo (relazione tra memo e tag).

Una volta creato il database e testato, è stata realizzata la parte grafica.
L’applicazione è dotata di 4 funzioni: 
    • Aggiungere memo
    • Eliminare memo
    • Eliminare tutti i memo
    • Ricaricare i memo
Successivamente è stata aggiunta una tabella Account al database che contiene gli utenti.
Per il server si è optato per json-server, in cui vengono caricati gli utenti, tutto il resto viene salvato sul database locale.
