import 'dart:convert';

import 'package:floor/floor.dart';

@entity
class Memo {
  @PrimaryKey(autoGenerate: true)
  final int memoID;
  final String title;
  final String body;
  final int categoryID;
  final int userID;

  Memo(
    this.memoID,
    this.title,
    this.body,
    this.categoryID,
    this.userID,
  );

  int getID() {
    return this.memoID;
  }

  String toJson() {
    return jsonEncode({
      'id': memoID,
      'title': title,
      'body': body,
      'categoryID': categoryID,
      'userID': userID
    });
  }

  factory Memo.fromJson(Map<String, dynamic> json) {
    print(json['id']);
    return Memo(json['id'], json['title'], json['body'], json['categoryID'],
        json['userID']);
  }
}
