import 'dart:convert';

import 'package:floor/floor.dart';

@entity
class Account {
  @PrimaryKey(autoGenerate: true)
  final int accountID;
  final String email;
  final String password;

  Account(this.accountID, this.email, this.password);

  String toJson() {
    return jsonEncode({'id': accountID, 'email': email, 'password': password});
  }

  factory Account.fromJson(Map<String, dynamic> json) {
    return Account(json['id'], json['email'], json['password']);
  }
}
