import 'dart:convert';

import 'package:floor/floor.dart';

@entity
class Category {
  @PrimaryKey(autoGenerate: true)
  final int categoryID;
  final String category;

  Category(this.categoryID, this.category);

  String toJson() {
    return jsonEncode({'id': categoryID, 'category': category});
  }

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(json['id'], json['category']);
  }
}
