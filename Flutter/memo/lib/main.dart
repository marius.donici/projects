import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:memo/account.dart';
import 'package:memo/category.dart';
import 'package:memo/category_dao.dart';

import 'package:memo/memo.preview.dart';
import 'package:memo/memo_screen.dart';
import 'package:memo/shared_dao.dart';
import 'package:memo/tag_dao.dart';
import 'package:memo/tagged_memo.dart';
import 'package:memo/tagged_memo_dao.dart';

import 'package:http/http.dart' as http;

import 'accound_dao.dart';
import 'database.dart';
import 'memo.dart';
import 'memo_dao.dart';
import 'shared.dart';
import 'tag.dart';

/*main(List<String> args) {
  runApp(MyApp());
*/
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState(title);
}

class _MyHomePageState extends State<MyHomePage> {
  late MemoDao _dao;
  late CategoryDao _daoC;
  late TagDao _daoT;
  late TaggedMemoDao _daoTG;
  late AccountDao _daoAC;
  late SharedDao _sharedDao;

  var _memos = <Memo>[];
  var _categories = <Category>[];
  var _taggedMemos = <TaggedMemo>[];
  var _tags = <Tag>[];
  var _shareds = <Shared>[];

  final titleController = TextEditingController();
  final bodyController = TextEditingController();
  final categoryController = TextEditingController();
  final tagsController = TextEditingController();
  final email = TextEditingController();
  String user = "";

  String _categoryToPass = "AAA";

  List<String> _tagsToPass = <String>[];

  _MyHomePageState(String u) {
    user = u;
    print(user);
  }

  @override
  void initState() {
    super.initState();
    prepareAll();
  }

  void prepareAll() async {
    await _getDaos();
    /*
    await _dao.deleteMemos(_memos);
    await _daoC.deleteCategories(_categories);
    await _daoT.deleteTags(_tags);
    await _daoTG.deleteTaggedMemos(_taggedMemos);*/
    /*await updateCategories();
    await updateMemos();
    await updateTags();
    await updateTaggedMemos();

    await getDataFromServer();
    print("Aggiorno le liste");*/
    await updateCategories();
    await updateMemos();
    await updateTags();
    await updateTaggedMemos();
    print("Ho aggiornato le liste");
  }

  Future<void> updateCategories() async {
    _categories = await _daoC.getCategories();
    setState(() {});
  }

  Future<void> updateMemos() async {
    Account? id = await _daoAC.getIDAccount(user);
    if (id != null) {
      print(id);
      _memos = await _dao.getMemos(id.accountID);
    }

    setState(() {});
    print(_memos);
  }

  Future<void> updateTaggedMemos() async {
    _taggedMemos = await _daoTG.getTaggedMEmos();
    setState(() {});
    print("TAGGEDMEMOSHERE " + _taggedMemos.toString());
  }

  Future<void> updateTags() async {
    _tags = await _daoT.getCategories();
    setState(() {});
  }

  Future<void> updateShareds() async {
    _shareds = await _sharedDao.getShareds();
    setState(() {});
  }

  Future<Null> _getDaos() async {
    print("Prendo i dao");
    AppDatabase database =
        await $FloorAppDatabase.databaseBuilder('app_database.db').build();
    print("");
    _dao = database.memoDao;
    _daoC = database.categoryDao;
    _daoT = database.tagDao;
    _daoTG = database.taggedMemoDao;
    _daoAC = database.accountDao;
    _sharedDao = database.sharedDao;

    print("Ho preso i dao");

    /*updateCategories();
    updateMemos();
    updateTaggedMemos();
    updateTags();*/
  }

  Future<void> getDataFromServer() async {
    print("Prendo i dati dal server");

    /*int i = 0;
    if (_memos.length > 0) {
      i = _memos.last.memoID + 1;
    }
    var response =
        await http.get(Uri.http('10.0.2.2:3000', 'memo/${i.toString()}'));

    while (response.statusCode == 200) {
      await _dao.insertMemo(Memo.fromJson(jsonDecode(response.body)));
      i++;
      response =
          await http.get(Uri.http('10.0.2.2:3000', 'memo/${i.toString()}'));
    }

    if (_categories.length > 0) {
      i = _categories.last.categoryID + 1;
    }

    i = 0;
    response =
        await http.get(Uri.http('10.0.2.2:3000', 'category/${i.toString()}'));
    while (response.statusCode == 200) {
      await _daoC.insertCategory(Category.fromJson(jsonDecode(response.body)));
      i++;
      response =
          await http.get(Uri.http('10.0.2.2:3000', 'category/${i.toString()}'));
    }

    if (_tags.length > 0) {
      i = _tags.last.tagID + 1;
    }

    i = 0;
    response = await http.get(Uri.http('10.0.2.2:3000', 'tag/${i.toString()}'));
    while (response.statusCode == 200) {
      await _daoT.insertTag(Tag.fromJson(jsonDecode(response.body)));
      i++;
      response =
          await http.get(Uri.http('10.0.2.2:3000', 'tag/${i.toString()}'));
    }

    if (_taggedMemos.length > 0) {
      i = _taggedMemos.last.tgID + 1;
    }

    i = 0;
    response = await http
        .get(Uri.http('10.0.2.2:3000', 'taggedmemos/${i.toString()}'));
    while (response.statusCode == 200) {
      await _daoTG
          .insertTaggedMemo(TaggedMemo.fromJson(jsonDecode(response.body)));
      i++;
      response = await http
          .get(Uri.http('10.0.2.2:3000', 'taggedmemos/${i.toString()}'));
    }

    if (_shareds.length > 0) {
      i = _shareds.last.sharedID + 1;
    }

    i = 0;
    response =
        await http.get(Uri.http('10.0.2.2:3000', 'shared/${i.toString()}'));

    while (response.statusCode == 200 && response.body.length > 0) {
      await _sharedDao.insertShared(Shared.fromJson(jsonDecode(response.body)));
      Shared sh = Shared.fromJson(jsonDecode(response.body));
      Memo? m = _dao.getMemo(sh.memoID) as Memo?;
      if (m != null) {
        Account? user = await _daoAC.getIDAccount(this.user);
        if (user != null) {
          if (user.accountID == sh.recipientID) {
            _memos.add(m);
            setState(() {});
          }
        }
      }
      i++;
      response =
          await http.get(Uri.http('10.0.2.2:3000', 'shared/${i.toString()}'));
    }

    print("Ho preso i dati dal server");*/
  }

  void getCategory(Memo m) {
    for (Category c in _categories) {
      if (c.categoryID == m.categoryID) {
        _categoryToPass = c.category;
        break;
      } else {
        _categoryToPass = "ERROR";
      }
    }
  }

  void getTags(Memo m) {
    for (TaggedMemo tm in _taggedMemos) {
      if (tm.memoID == m.memoID) {
        for (Tag t in _tags) {
          if (t.tagID == tm.tagID) {
            _tagsToPass.add(t.tag);
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final _controller = ScrollController();
    Timer(
      Duration(microseconds: 50),
      () => _controller.jumpTo(_controller.position.maxScrollExtent),
    );
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.fromLTRB(10.0, 25.0, 10.0, 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                child: Text(
                  "TUTTI I MEMO",
                  style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
                )),
            Divider(),
            SafeArea(
              child: Container(
                  height: (MediaQuery.of(context).size.height / 100) * 70,
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      controller: _controller,
                      itemCount: _memos.length,
                      itemBuilder: (BuildContext context, int index) {
                        getCategory(_memos[index]);
                        _tagsToPass.clear();

                        getTags(_memos[index]);
                        print(_tagsToPass);
                        print("BBB");

                        return Container(
                            child: Column(
                          children: [
                            Container(
                                key: Key(index.toString()),
                                child: Padding(
                                    padding: EdgeInsets.all(15.0),
                                    child: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.grey, width: 1.5),
                                            borderRadius:
                                                BorderRadius.circular(12)),
                                        child: Padding(
                                            padding: EdgeInsets.all(5.0),
                                            child: InkWell(
                                              onLongPress: () {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return AlertDialog(
                                                        content: Form(
                                                            child: TextButton(
                                                          child:
                                                              Text("Elimina"),
                                                          onPressed: () async {
                                                            _dao.deleteMemo(
                                                                _memos[index]);
                                                            updateMemos();
                                                            setState(() {});
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                        )),
                                                      );
                                                    });
                                              },
                                              onTap: () {
                                                print("TAPTAP");
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            MemoScreen(
                                                                memo: _memos[
                                                                    index],
                                                                category:
                                                                    _categoryToPass,
                                                                tags:
                                                                    _tagsToPass)));
                                              },
                                              child: MemoPreview(
                                                memo: _memos[index],
                                                category: _categoryToPass,
                                                tags: _tagsToPass,
                                              ),
                                            )))))
                          ],
                        ));
                      })),
            ),
            Spacer(),
            Container(
              height: (height / 100) * 10,
              child: ButtonBar(
                alignment: MainAxisAlignment.center,
                children: <Widget>[
                  FloatingActionButton(
                    child: Icon(Icons.delete),
                    onPressed: () async {
                      deleteMemos();
                    },
                  ),
                  FloatingActionButton(
                      child: Icon(Icons.refresh),
                      onPressed: () async {
                        //deleteMemos();
                        //await getDataFromServer();
                        await updateCategories();
                        await updateMemos();
                        await updateTaggedMemos();
                        await updateTags();
                      }),
                  FloatingActionButton(
                    child: Icon(Icons.add),
                    onPressed: () async {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              content: Form(
                                child: Column(
                                  children: [
                                    Text(
                                      "Aggiungi memo",
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    Divider(),
                                    Container(
                                      child: TextFormField(
                                        autofocus: true,
                                        controller: titleController,
                                        decoration: InputDecoration(
                                          hintText: "titolo",
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: TextFormField(
                                        controller: bodyController,
                                        decoration: InputDecoration(
                                            hintText: "testo",
                                            border: InputBorder.none),
                                      ),
                                    ),
                                    Container(
                                      child: TextFormField(
                                        controller: categoryController,
                                        decoration: InputDecoration(
                                            hintText: "categoria",
                                            border: InputBorder.none),
                                      ),
                                    ),
                                    Container(
                                      child: TextFormField(
                                        controller: tagsController,
                                        decoration: InputDecoration(
                                            hintText: "tags",
                                            border: InputBorder.none),
                                      ),
                                    ),
                                    Spacer(),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width,
                                      height: 50,
                                      child: TextButton(
                                        child: Text("Aggiungi memo"),
                                        onPressed: () async {
                                          handleCategory();
                                          handleMemo();
                                          print("AAAA");
                                          handleTags();
                                          Navigator.pop(context);
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void handleCategory() async {
    Category? c = await _daoC.getMaxCategory();
    Category cat;

    if (c != null) {
      cat = Category(c.categoryID + 1, categoryController.text);
      await _daoC.insertCategory(cat);
      /*await http.post(Uri.http('10.0.2.2:3000', 'category'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: cat.toJson());*/
    } else {
      await _daoC.insertCategory(Category(1, categoryController.text));
    }
    updateCategories();
  }

  void handleMemo() async {
    Memo? m = await _dao.getMaxMemo();
    Category? c = await _daoC.getMaxCategory();

    Memo memo;
    print("user $user");
    Account? userID = await _daoAC.getIDAccount(user);

    if (m != null && userID != null) {
      if (c != null) {
        memo = Memo(m.getID() + 1, titleController.text, bodyController.text,
            c.categoryID, userID.accountID);
        await _dao.insertMemo(memo);
        /*await http.post(
            Uri.http(
              '10.0.2.2:3000',
              'memo',
            ),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: memo.toJson());*/
      }
    } else {
      if (c != null && userID != null) {
        await _dao.insertMemo(Memo(1, titleController.text, bodyController.text,
            c.categoryID, userID.accountID));
      }
    }
    updateMemos();
  }

  void handleTags() async {
    String tgs = tagsController.text.replaceAll(' ', '');
    List<String> tags = tgs.split('#');
    print(tags);
    Tag? t = await _daoT.getMaxTag();

    Tag tag;
    TaggedMemo tm;

    if (t != null) {
      int m = t.tagID;
      for (int i = 0; i < tags.length; i++) {
        if (tags[i].length > 0) {
          tag = Tag(m + (i + 1), tags[i]);
          await _daoT.insertTag(tag);
          /*await http.post(Uri.http('10.0.2.2:3000', 'tag'),
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
              },
              body: tag.toJson());*/
          Memo? k = await _dao.getMaxMemo();
          if (k != null) {
            print(k.memoID);
            tm = TaggedMemo(_taggedMemos.length + i, k.memoID, m + (i + 1));
            await _daoTG.insertTaggedMemo(tm);
            /*await http.post(Uri.http('10.0.2.2:3000', 'taggedmemos'),
                headers: <String, String>{
                  'Content-Type': 'application/json; charset=UTF-8',
                },
                body: tm.toJson());*/
          } else {
            print("***");
          }
        }
      }
    }

    await updateTags();
    await updateTaggedMemos();
  }

  void deleteMemos() async {
    await _dao.deleteMemos(_memos);
    await _daoC.deleteCategories(_categories);
    await _daoT.deleteTags(_tags);
    await _daoTG.deleteTaggedMemos(_taggedMemos);
    updateMemos();
    updateCategories();
    updateTaggedMemos();
    updateTags();
  }
}
