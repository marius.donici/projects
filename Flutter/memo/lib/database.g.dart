// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  MemoDao? _memoDaoInstance;

  CategoryDao? _categoryDaoInstance;

  TagDao? _tagDaoInstance;

  TaggedMemoDao? _taggedMemoDaoInstance;

  AccountDao? _accountDaoInstance;

  SharedDao? _sharedDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Memo` (`memoID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `title` TEXT NOT NULL, `body` TEXT NOT NULL, `categoryID` INTEGER NOT NULL, `userID` INTEGER NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Category` (`categoryID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `category` TEXT NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Tag` (`tagID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `tag` TEXT NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `TaggedMemo` (`tgID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `memoID` INTEGER NOT NULL, `tagID` INTEGER NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Account` (`accountID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `email` TEXT NOT NULL, `password` TEXT NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Shared` (`sharedID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `memoID` INTEGER NOT NULL, `recipientID` INTEGER NOT NULL)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  MemoDao get memoDao {
    return _memoDaoInstance ??= _$MemoDao(database, changeListener);
  }

  @override
  CategoryDao get categoryDao {
    return _categoryDaoInstance ??= _$CategoryDao(database, changeListener);
  }

  @override
  TagDao get tagDao {
    return _tagDaoInstance ??= _$TagDao(database, changeListener);
  }

  @override
  TaggedMemoDao get taggedMemoDao {
    return _taggedMemoDaoInstance ??= _$TaggedMemoDao(database, changeListener);
  }

  @override
  AccountDao get accountDao {
    return _accountDaoInstance ??= _$AccountDao(database, changeListener);
  }

  @override
  SharedDao get sharedDao {
    return _sharedDaoInstance ??= _$SharedDao(database, changeListener);
  }
}

class _$MemoDao extends MemoDao {
  _$MemoDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _memoInsertionAdapter = InsertionAdapter(
            database,
            'Memo',
            (Memo item) => <String, Object?>{
                  'memoID': item.memoID,
                  'title': item.title,
                  'body': item.body,
                  'categoryID': item.categoryID,
                  'userID': item.userID
                }),
        _memoDeletionAdapter = DeletionAdapter(
            database,
            'Memo',
            ['memoID'],
            (Memo item) => <String, Object?>{
                  'memoID': item.memoID,
                  'title': item.title,
                  'body': item.body,
                  'categoryID': item.categoryID,
                  'userID': item.userID
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<Memo> _memoInsertionAdapter;

  final DeletionAdapter<Memo> _memoDeletionAdapter;

  @override
  Future<List<Memo>> getMemos(int user) async {
    return _queryAdapter.queryList('SELECT * FROM Memo WHERE userID = ?',
        arguments: [user],
        mapper: (Map<String, Object?> row) => Memo(
            row['memoID'] as int,
            row['title'] as String,
            row['body'] as String,
            row['categoryID'] as int,
            row['userID'] as int));
  }

  @override
  Future<Memo?> getMaxMemo() async {
    return _queryAdapter.query(
        'SELECT * FROM Memo ORDER BY memoID DESC LIMIT 1',
        mapper: (Map<String, Object?> row) => Memo(
            row['memoID'] as int,
            row['title'] as String,
            row['body'] as String,
            row['categoryID'] as int,
            row['userID'] as int));
  }

  @override
  Future<Memo?> getMemo(int id) async {
    return _queryAdapter.query('SELECT * FROM Memo WHERE memoID = ? LIMIT 1',
        arguments: [id],
        mapper: (Map<String, Object?> row) => Memo(
            row['memoID'] as int,
            row['title'] as String,
            row['body'] as String,
            row['categoryID'] as int,
            row['userID'] as int));
  }

  @override
  Future<void> insertMemo(Memo memo) async {
    await _memoInsertionAdapter.insert(memo, OnConflictStrategy.abort);
  }

  @override
  Future<List<int>> insertMemos(List<Memo> memos) {
    return _memoInsertionAdapter.insertListAndReturnIds(
        memos, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteMemo(Memo m) async {
    await _memoDeletionAdapter.delete(m);
  }

  @override
  Future<int> deleteMemos(List<Memo> memos) {
    return _memoDeletionAdapter.deleteListAndReturnChangedRows(memos);
  }
}

class _$CategoryDao extends CategoryDao {
  _$CategoryDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _categoryInsertionAdapter = InsertionAdapter(
            database,
            'Category',
            (Category item) => <String, Object?>{
                  'categoryID': item.categoryID,
                  'category': item.category
                }),
        _categoryDeletionAdapter = DeletionAdapter(
            database,
            'Category',
            ['categoryID'],
            (Category item) => <String, Object?>{
                  'categoryID': item.categoryID,
                  'category': item.category
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<Category> _categoryInsertionAdapter;

  final DeletionAdapter<Category> _categoryDeletionAdapter;

  @override
  Future<List<Category>> getCategories() async {
    return _queryAdapter.queryList('SELECT * FROM Category',
        mapper: (Map<String, Object?> row) =>
            Category(row['categoryID'] as int, row['category'] as String));
  }

  @override
  Future<Category?> getCategoryByID(int id) async {
    return _queryAdapter.query('SELECT * FROM Category WHERE categoryID = ?',
        arguments: [id],
        mapper: (Map<String, Object?> row) =>
            Category(row['categoryID'] as int, row['category'] as String));
  }

  @override
  Future<Category?> getCategoryByName(String category) async {
    return _queryAdapter.query('SELECT * FROM Category WHERE category = ?',
        arguments: [category],
        mapper: (Map<String, Object?> row) =>
            Category(row['categoryID'] as int, row['category'] as String));
  }

  @override
  Future<Category?> getMaxCategory() async {
    return _queryAdapter.query(
        'SELECT * FROM Category ORDER BY categoryID DESC LIMIT 1',
        mapper: (Map<String, Object?> row) =>
            Category(row['categoryID'] as int, row['category'] as String));
  }

  @override
  Future<void> insertCategory(Category category) async {
    await _categoryInsertionAdapter.insert(category, OnConflictStrategy.abort);
  }

  @override
  Future<List<int>> insertCategories(List<Category> categories) {
    return _categoryInsertionAdapter.insertListAndReturnIds(
        categories, OnConflictStrategy.abort);
  }

  @override
  Future<int> deleteCategories(List<Category> memos) {
    return _categoryDeletionAdapter.deleteListAndReturnChangedRows(memos);
  }
}

class _$TagDao extends TagDao {
  _$TagDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _tagInsertionAdapter = InsertionAdapter(
            database,
            'Tag',
            (Tag item) =>
                <String, Object?>{'tagID': item.tagID, 'tag': item.tag}),
        _tagDeletionAdapter = DeletionAdapter(
            database,
            'Tag',
            ['tagID'],
            (Tag item) =>
                <String, Object?>{'tagID': item.tagID, 'tag': item.tag});

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<Tag> _tagInsertionAdapter;

  final DeletionAdapter<Tag> _tagDeletionAdapter;

  @override
  Future<List<Tag>> getCategories() async {
    return _queryAdapter.queryList('SELECT * FROM Tag',
        mapper: (Map<String, Object?> row) =>
            Tag(row['tagID'] as int, row['tag'] as String));
  }

  @override
  Future<Tag?> getTagByID(int id) async {
    return _queryAdapter.query('SELECT * FROM Tag WHERE TagID = ?',
        arguments: [id],
        mapper: (Map<String, Object?> row) =>
            Tag(row['tagID'] as int, row['tag'] as String));
  }

  @override
  Future<Tag?> getTagByName(String tag) async {
    return _queryAdapter.query('SELECT * FROM Tag WHERE Tag = ?',
        arguments: [tag],
        mapper: (Map<String, Object?> row) =>
            Tag(row['tagID'] as int, row['tag'] as String));
  }

  @override
  Future<Tag?> getMaxTag() async {
    return _queryAdapter.query('SELECT * FROM Tag ORDER BY TagID DESC LIMIT 1',
        mapper: (Map<String, Object?> row) =>
            Tag(row['tagID'] as int, row['tag'] as String));
  }

  @override
  Future<void> insertTag(Tag tag) async {
    await _tagInsertionAdapter.insert(tag, OnConflictStrategy.abort);
  }

  @override
  Future<List<int>> insertTags(List<Tag> tags) {
    return _tagInsertionAdapter.insertListAndReturnIds(
        tags, OnConflictStrategy.abort);
  }

  @override
  Future<int> deleteTags(List<Tag> tags) {
    return _tagDeletionAdapter.deleteListAndReturnChangedRows(tags);
  }
}

class _$TaggedMemoDao extends TaggedMemoDao {
  _$TaggedMemoDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _taggedMemoInsertionAdapter = InsertionAdapter(
            database,
            'TaggedMemo',
            (TaggedMemo item) => <String, Object?>{
                  'tgID': item.tgID,
                  'memoID': item.memoID,
                  'tagID': item.tagID
                }),
        _taggedMemoDeletionAdapter = DeletionAdapter(
            database,
            'TaggedMemo',
            ['tgID'],
            (TaggedMemo item) => <String, Object?>{
                  'tgID': item.tgID,
                  'memoID': item.memoID,
                  'tagID': item.tagID
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<TaggedMemo> _taggedMemoInsertionAdapter;

  final DeletionAdapter<TaggedMemo> _taggedMemoDeletionAdapter;

  @override
  Future<List<TaggedMemo>> getTaggedMEmos() async {
    return _queryAdapter.queryList('SELECT * FROM TaggedMemo',
        mapper: (Map<String, Object?> row) => TaggedMemo(
            row['tgID'] as int, row['memoID'] as int, row['tagID'] as int));
  }

  @override
  Future<TaggedMemo?> getTaggedMemoByID(int id) async {
    return _queryAdapter.query('SELECT * FROM TaggedMemo WHERE tgID = ?',
        arguments: [id],
        mapper: (Map<String, Object?> row) => TaggedMemo(
            row['tgID'] as int, row['memoID'] as int, row['tagID'] as int));
  }

  @override
  Future<TaggedMemo?> getMaxTaggedMemo() async {
    return _queryAdapter.query(
        'SELECT * FROM TaggedMemo ORDER BY tgID DESC LIMIT 1',
        mapper: (Map<String, Object?> row) => TaggedMemo(
            row['tgID'] as int, row['memoID'] as int, row['tagID'] as int));
  }

  @override
  Future<void> insertTaggedMemo(TaggedMemo taggedMemo) async {
    await _taggedMemoInsertionAdapter.insert(
        taggedMemo, OnConflictStrategy.abort);
  }

  @override
  Future<List<int>> insertTaggedMemos(List<TaggedMemo> tmemos) {
    return _taggedMemoInsertionAdapter.insertListAndReturnIds(
        tmemos, OnConflictStrategy.abort);
  }

  @override
  Future<int> deleteTaggedMemos(List<TaggedMemo> tmemo) {
    return _taggedMemoDeletionAdapter.deleteListAndReturnChangedRows(tmemo);
  }
}

class _$AccountDao extends AccountDao {
  _$AccountDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _accountInsertionAdapter = InsertionAdapter(
            database,
            'Account',
            (Account item) => <String, Object?>{
                  'accountID': item.accountID,
                  'email': item.email,
                  'password': item.password
                }),
        _accountDeletionAdapter = DeletionAdapter(
            database,
            'Account',
            ['accountID'],
            (Account item) => <String, Object?>{
                  'accountID': item.accountID,
                  'email': item.email,
                  'password': item.password
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<Account> _accountInsertionAdapter;

  final DeletionAdapter<Account> _accountDeletionAdapter;

  @override
  Future<List<Account>> getAccounts() async {
    return _queryAdapter.queryList('SELECT * FROM Account',
        mapper: (Map<String, Object?> row) => Account(row['accountID'] as int,
            row['email'] as String, row['password'] as String));
  }

  @override
  Future<Account?> getAccount(String email) async {
    return _queryAdapter.query('SELECT * FROM Account WHERE email = ?',
        arguments: [email],
        mapper: (Map<String, Object?> row) => Account(row['accountID'] as int,
            row['email'] as String, row['password'] as String));
  }

  @override
  Future<Account?> getIDAccount(String email) async {
    return _queryAdapter.query('SELECT * FROM Account where email = ? LIMIT 1',
        arguments: [email],
        mapper: (Map<String, Object?> row) => Account(row['accountID'] as int,
            row['email'] as String, row['password'] as String));
  }

  @override
  Future<void> insertAccount(Account acc) async {
    await _accountInsertionAdapter.insert(acc, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteAccounts(List<Account> accounts) async {
    await _accountDeletionAdapter.deleteList(accounts);
  }
}

class _$SharedDao extends SharedDao {
  _$SharedDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _sharedInsertionAdapter = InsertionAdapter(
            database,
            'Shared',
            (Shared item) => <String, Object?>{
                  'sharedID': item.sharedID,
                  'memoID': item.memoID,
                  'recipientID': item.recipientID
                }),
        _sharedDeletionAdapter = DeletionAdapter(
            database,
            'Shared',
            ['sharedID'],
            (Shared item) => <String, Object?>{
                  'sharedID': item.sharedID,
                  'memoID': item.memoID,
                  'recipientID': item.recipientID
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<Shared> _sharedInsertionAdapter;

  final DeletionAdapter<Shared> _sharedDeletionAdapter;

  @override
  Future<List<Shared>> getShareds() async {
    return _queryAdapter.queryList('SELECT * FROM Shared',
        mapper: (Map<String, Object?> row) => Shared(row['sharedID'] as int,
            row['memoID'] as int, row['recipientID'] as int));
  }

  @override
  Future<Shared?> getSharedByID(int id) async {
    return _queryAdapter.query('SELECT * FROM Shared WHERE sharedID = ?',
        arguments: [id],
        mapper: (Map<String, Object?> row) => Shared(row['sharedID'] as int,
            row['memoID'] as int, row['recipientID'] as int));
  }

  @override
  Future<Shared?> getMaxShared() async {
    return _queryAdapter.query(
        'SELECT * FROM Shared ORDER BY tgID DESC LIMIT 1',
        mapper: (Map<String, Object?> row) => Shared(row['sharedID'] as int,
            row['memoID'] as int, row['recipientID'] as int));
  }

  @override
  Future<void> insertShared(Shared shared) async {
    await _sharedInsertionAdapter.insert(shared, OnConflictStrategy.abort);
  }

  @override
  Future<List<int>> insertShareds(List<Shared> shareds) {
    return _sharedInsertionAdapter.insertListAndReturnIds(
        shareds, OnConflictStrategy.abort);
  }

  @override
  Future<int> deleteShareds(List<Shared> shareds) {
    return _sharedDeletionAdapter.deleteListAndReturnChangedRows(shareds);
  }
}
