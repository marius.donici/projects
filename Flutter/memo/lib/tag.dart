import 'dart:convert';

import 'package:floor/floor.dart';

@entity
class Tag {
  @PrimaryKey(autoGenerate: true)
  final int tagID;
  final String tag;

  Tag(this.tagID, this.tag);

  String toJson() {
    return jsonEncode({'id': tagID, 'tag': tag});
  }

  factory Tag.fromJson(Map<String, dynamic> json) {
    return Tag(json['id'], json['tag']);
  }
}
