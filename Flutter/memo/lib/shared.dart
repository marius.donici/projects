import 'dart:convert';

import 'package:floor/floor.dart';

@entity
class Shared {
  @PrimaryKey(autoGenerate: true)
  final int sharedID;
  final int memoID;
  final int recipientID;

  Shared(this.sharedID, this.memoID, this.recipientID);

  String toJson() {
    return jsonEncode(
        {'id': sharedID, 'memoID': memoID, 'recipientID': recipientID});
  }

  factory Shared.fromJson(Map<String, dynamic> json) {
    return Shared(json['id'], json['memoID'], json['recipeintID']);
  }
}
