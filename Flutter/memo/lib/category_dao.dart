import 'package:floor/floor.dart';
import 'package:memo/category.dart';

@dao
abstract class CategoryDao {
  @Query('SELECT * FROM Category')
  Future<List<Category>> getCategories();
  @Query('SELECT * FROM Category WHERE categoryID = :id')
  Future<Category?> getCategoryByID(int id);
  @Query('SELECT * FROM Category WHERE category = :category')
  Future<Category?> getCategoryByName(String category);
  @Query('SELECT * FROM Category ORDER BY categoryID DESC LIMIT 1')
  Future<Category?> getMaxCategory();
  @insert
  Future<void> insertCategory(Category category);
  @insert
  Future<List<int>> insertCategories(List<Category> categories);
  @delete
  Future<int> deleteCategories(List<Category> memos);
}
