import 'package:floor/floor.dart';
import 'package:memo/accound_dao.dart';
import 'package:memo/account.dart';

import 'package:memo/memo.dart';
import 'package:memo/memo_dao.dart';
import 'package:memo/shared.dart';
import 'package:memo/shared_dao.dart';
import 'package:memo/tag.dart';
import 'package:memo/tag_dao.dart';
import 'package:memo/tagged_memo.dart';
import 'package:memo/tagged_memo_dao.dart';

import 'dart:async';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'category.dart';
import 'category_dao.dart';

part 'database.g.dart';

@Database(
    version: 1, entities: [Memo, Category, Tag, TaggedMemo, Account, Shared])
abstract class AppDatabase extends FloorDatabase {
  MemoDao get memoDao;
  CategoryDao get categoryDao;
  TagDao get tagDao;
  TaggedMemoDao get taggedMemoDao;
  AccountDao get accountDao;
  SharedDao get sharedDao;
}
