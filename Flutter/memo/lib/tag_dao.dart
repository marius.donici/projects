import 'package:floor/floor.dart';

import 'tag.dart';

@dao
abstract class TagDao {
  @Query('SELECT * FROM Tag')
  Future<List<Tag>> getCategories();

  @Query('SELECT * FROM Tag WHERE TagID = :id')
  Future<Tag?> getTagByID(int id);

  @Query('SELECT * FROM Tag WHERE Tag = :tag')
  Future<Tag?> getTagByName(String tag);

  @Query('SELECT * FROM Tag ORDER BY TagID DESC LIMIT 1')
  Future<Tag?> getMaxTag();

  @insert
  Future<void> insertTag(Tag tag);
  @delete
  Future<int> deleteTags(List<Tag> tags);

  @insert
  Future<List<int>> insertTags(List<Tag> tags);
}
