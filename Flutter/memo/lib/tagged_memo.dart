import 'dart:convert';

import 'package:floor/floor.dart';

@entity
class TaggedMemo {
  @PrimaryKey(autoGenerate: true)
  final int tgID;
  final int memoID;
  final int tagID;

  TaggedMemo(this.tgID, this.memoID, this.tagID);

  String toJson() {
    return jsonEncode({'id': tgID, 'memoID': memoID, 'tagID': tagID});
  }

  factory TaggedMemo.fromJson(Map<String, dynamic> json) {
    return TaggedMemo(json['id'], json['memoID'], json['tagID']);
  }
}
