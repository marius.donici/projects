import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:memo/accound_dao.dart';
import 'package:memo/account.dart';
import 'package:memo/main.dart';
import 'package:http/http.dart' as http;
import 'database.dart';

main(List<String> args) {
  runApp(Login());
}

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoginScreen(),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late AccountDao _daoAC;

  List<Account> _accs = [];

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _password2Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    prepareAll();
  }

  void prepareAll() async {
    await _getDaos();
    await updateAccounts();
    print(_accs);
    await getDataFromServer();
    await updateAccounts();
    print(_accs);
  }

  Future<Null> _getDaos() async {
    AppDatabase database =
        await $FloorAppDatabase.databaseBuilder('app_database.db').build();
    _daoAC = database.accountDao;
  }

  Future<void> getDataFromServer() async {
    int i = 1;
    if (_accs.length > 0) {
      i = _accs.last.accountID + 1;
      print(i);
    }

    var response =
        await http.get(Uri.http('10.0.2.2:3000', 'account/${i.toString()}'));

    while (response.statusCode == 200) {
      try {
        await _daoAC.insertAccount(Account.fromJson(jsonDecode(response.body)));
      } catch (e) {
        print(e);
      }

      i++;
      response =
          await http.get(Uri.http('10.0.2.2:3000', 'account/${i.toString()}'));
    }
  }

  Future<void> updateAccounts() async {
    _accs = await _daoAC.getAccounts();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.all(20.0),
            child: Text(
              "Memo App",
              style: TextStyle(fontSize: 40.0),
            ),
          ),
          Divider(),
          Container(
              margin: EdgeInsets.all(20.0),
              child: TextFormField(
                controller: _emailController,
              )),
          Container(
              margin: EdgeInsets.all(20.0),
              child: TextFormField(
                controller: _passwordController,
              )),
          Container(
              margin: EdgeInsets.all(20.0),
              child: TextButton(
                  onPressed: () async {
                    for (Account a in _accs) {
                      if (a.email == _emailController.text &&
                          a.password == _passwordController.text) {
                        //_emailController.clear();
                        _passwordController.clear();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    MyHomePage(title: _emailController.text)));
                      }
                    }
                  },
                  child: Text("Entra"))),
          Container(
              margin: EdgeInsets.all(20.0),
              child: TextButton(
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            content: Form(
                                child: Column(
                              children: [
                                Container(
                                    margin: EdgeInsets.all(20.0),
                                    child: TextFormField(
                                      controller: _emailController,
                                    )),
                                Container(
                                    margin: EdgeInsets.all(20.0),
                                    child: TextFormField(
                                      controller: _passwordController,
                                    )),
                                Container(
                                    margin: EdgeInsets.all(20.0),
                                    child: TextFormField(
                                      controller: _password2Controller,
                                    )),
                                TextButton(
                                    onPressed: () async {
                                      if (_password2Controller.text ==
                                          _passwordController.text) {
                                        if (_accs.isNotEmpty) {
                                          for (Account a in _accs) {
                                            if (a.email ==
                                                _emailController.text) {
                                              break;
                                            }
                                            Account acc = Account(
                                                _accs.last.accountID + 1,
                                                _emailController.text,
                                                _passwordController.text);
                                            await _daoAC.insertAccount(acc);
                                            await http.post(
                                                Uri.http(
                                                    '10.0.2.2:3000', 'account'),
                                                headers: <String, String>{
                                                  'Content-Type':
                                                      'application/json; charset=UTF-8',
                                                },
                                                body: acc.toJson());
                                          }
                                        }
                                        /*else {
                                          await _daoAC.insertAccount(Account(
                                              1,
                                              _emailController.text,
                                              _passwordController.text));
                                        }*/

                                        _accs = await _daoAC.getAccounts();
                                        setState(() {});
                                        _emailController.clear();
                                        _password2Controller.clear();
                                        _passwordController.clear();
                                        Navigator.pop(context);
                                        print(_accs);
                                      }
                                    },
                                    child: Text("Registrati")),
                              ],
                            )),
                          );
                        });
                  },
                  child: Text("Registrati"))),
          Container(
            margin: EdgeInsets.all(20.0),
            child: Text(
              "Marius Donici",
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w100),
            ),
          )
        ],
      ),
    );
  }
}
