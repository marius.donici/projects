import 'package:floor/floor.dart';

import 'shared.dart';

@dao
abstract class SharedDao {
  @Query('SELECT * FROM Shared')
  Future<List<Shared>> getShareds();
  @Query('SELECT * FROM Shared WHERE sharedID = :id')
  Future<Shared?> getSharedByID(int id);

  @Query('SELECT * FROM Shared ORDER BY tgID DESC LIMIT 1')
  Future<Shared?> getMaxShared();
  @insert
  Future<void> insertShared(Shared shared);
  @delete
  Future<int> deleteShareds(List<Shared> shareds);
  @insert
  Future<List<int>> insertShareds(List<Shared> shareds);
}
