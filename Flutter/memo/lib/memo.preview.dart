import 'dart:ui';

import 'package:flutter/material.dart';

import 'memo.dart';

class MemoPreview extends StatelessWidget {
  MemoPreview({required this.memo, required this.category, required this.tags});

  final Memo memo;
  final String category;
  final List<String> tags;
  @override
  Widget build(BuildContext context) {
    int max = 0;
    if (memo.body.length > 10) {
      max = 10;
    } else {
      max = memo.body.length;
    }
    return Container(
      child: Column(
        children: [
          Text(
            category.toString(),
            style: TextStyle(fontSize: 15.0),
          ),
          Text(
            memo.title,
            style: TextStyle(fontSize: 25.0),
          ),
          Text(
            memo.body.substring(0, max) + "...",
            style: TextStyle(fontSize: 20.0),
          ),
          Container(
            height: 20,
            width: 100,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: tags.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  child: Row(
                    children: [Text("#"), Text(tags[index]), Text(" ")],
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
