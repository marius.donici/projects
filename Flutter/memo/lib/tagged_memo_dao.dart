import 'package:floor/floor.dart';

import 'tagged_memo.dart';

@dao
abstract class TaggedMemoDao {
  @Query('SELECT * FROM TaggedMemo')
  Future<List<TaggedMemo>> getTaggedMEmos();
  @Query('SELECT * FROM TaggedMemo WHERE tgID = :id')
  Future<TaggedMemo?> getTaggedMemoByID(int id);

  @Query('SELECT * FROM TaggedMemo ORDER BY tgID DESC LIMIT 1')
  Future<TaggedMemo?> getMaxTaggedMemo();
  @insert
  Future<void> insertTaggedMemo(TaggedMemo taggedMemo);
  @delete
  Future<int> deleteTaggedMemos(List<TaggedMemo> tmemo);
  @insert
  Future<List<int>> insertTaggedMemos(List<TaggedMemo> tmemos);
}
