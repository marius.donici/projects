import 'package:floor/floor.dart';
import 'package:memo/account.dart';

@dao
abstract class AccountDao {
  @Query('SELECT * FROM Account')
  Future<List<Account>> getAccounts();

  @Query('SELECT * FROM Account WHERE email = :email')
  Future<Account?> getAccount(String email);

  @Query('SELECT * FROM Account where email = :email LIMIT 1')
  Future<Account?> getIDAccount(String email);

  @insert
  Future<void> insertAccount(Account acc);
  @delete
  Future<void> deleteAccounts(List<Account> accounts);
}
