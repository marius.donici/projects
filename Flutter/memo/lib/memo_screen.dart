import 'package:flutter/material.dart';
import 'package:memo/memo.dart';

// ignore: must_be_immutable
class MemoScreen extends StatelessWidget {
  String category;

  Memo memo;

  List<String> tags;

  MemoScreen({required this.memo, required this.category, required this.tags});
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: [
          Padding(
              padding: EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 15.0),
              child: Text(
                category.toString(),
                style: TextStyle(fontSize: 20.0),
              )),
          Padding(
              padding: EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 15.0),
              child: Text(
                memo.title,
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
              )),
          Divider(),
          Padding(
              padding: EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 15.0),
              child: Text(
                memo.body,
                style: TextStyle(fontSize: 20.0),
              )),
          Expanded(
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: tags.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  child: Row(
                    children: [
                      Text("#", style: TextStyle(fontSize: 20.0)),
                      Text(tags[index], style: TextStyle(fontSize: 20.0)),
                      Text(" ", style: TextStyle(fontSize: 20.0))
                    ],
                  ),
                );
              },
            ),
          ),
          Spacer(),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: TextButton(
              child: Text("Vai ai memo"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          )
        ],
      ),
    );
  }
}
