import 'package:floor/floor.dart';

import 'memo.dart';

@dao
abstract class MemoDao {
  @Query('SELECT * FROM Memo WHERE userID = :user')
  Future<List<Memo>> getMemos(int user);

  @Query('SELECT * FROM Memo ORDER BY memoID DESC LIMIT 1')
  Future<Memo?> getMaxMemo();

  @Query('SELECT * FROM Memo WHERE memoID = :id LIMIT 1')
  Future<Memo?> getMemo(int id);

  @delete
  Future<void> deleteMemo(Memo m);

  @delete
  Future<int> deleteMemos(List<Memo> memos);

  @insert
  Future<void> insertMemo(Memo memo);

  @insert
  Future<List<int>> insertMemos(List<Memo> memos);
}
