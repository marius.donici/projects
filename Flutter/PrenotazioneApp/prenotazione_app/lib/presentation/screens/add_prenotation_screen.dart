import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:prenotazione_app/cubit/cubit/add_prenotation_cubit.dart';

// ignore: must_be_immutable
class AddPrenotationScreen extends StatelessWidget {
  final _room = TextEditingController();
  DateTime selectedDate = DateTime.now();

  String dropdownValue = "Prima ora";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Prenota aula"),
      ),
      body: BlocListener<AddPrenotationCubit, AddPrenotationState>(
        listener: (context, state) {
          if (state is PrenotationAdded) {
            Navigator.pop(context);
          }
        },
        child: Container(
          margin: EdgeInsets.all(20.0),
          child: _body(context),
        ),
      ),
    );
  }

  Widget _body(context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        TextField(
            autofocus: true,
            controller: _room,
            decoration: InputDecoration(hintText: "Numero aula")),
        InputDatePickerFormField(
          firstDate: DateTime(2021),
          lastDate: DateTime(2021, 12, 12),
          initialDate: selectedDate,
          onDateSubmitted: (date) {
            selectedDate = date;
          },
        ),
        DropdownButton<String>(
          value: dropdownValue,
          icon: Icon(Icons.arrow_downward),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.deepPurple),
          underline: Container(
            height: 2,
            color: Colors.deepPurpleAccent,
          ),
          onChanged: (String? newValue) {
            dropdownValue = newValue!;
          },
          items: <String>[
            'Prima ora',
            'Seconda ora',
            'Terza ora',
            'Quarta ora',
            'Quinta ora',
            'Sesta ora'
          ].map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
        Spacer(),
        InkWell(
          child: _addBtn(context),
          onTap: () => {
            BlocProvider.of<AddPrenotationCubit>(context).addPrenotation(
                _room.text.toString(),
                4,
                4,
                selectedDate.toString() + " $dropdownValue")
          },
        )
      ],
    );
  }

  Widget _addBtn(context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Center(
        child: BlocBuilder<AddPrenotationCubit, AddPrenotationState>(
          builder: (context, state) {
            if (state is AddingPrenotation) return CircularProgressIndicator();

            return Text(
              "Add Prenotation",
              style: TextStyle(color: Colors.white),
            );
          },
        ),
      ),
    );
  }
}
