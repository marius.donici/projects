import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prenotazione_app/costants/strings.dart';
import 'package:prenotazione_app/cubit/prenotations_cubit_cubit.dart';
import 'package:prenotazione_app/data/models/prenotation.dart';

class PrenotationsScreen extends StatelessWidget {
  final int id;

  const PrenotationsScreen({Key? key, required this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<PrenotationsCubitCubit>(context).fetchUserPrenotations(id);
    return Scaffold(
      appBar: AppBar(
        title: Text("Prenotazione aule"),
        actions: [
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              BlocProvider.of<PrenotationsCubitCubit>(context)
                  .fetchUserPrenotations(id);
            },
          )
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text("Aule prenotate"),
          BlocBuilder<PrenotationsCubitCubit, PrenotationsCubitState>(
              builder: (context, state) {
            if (!(state is PrenotationsLoaded))
              return Center(child: CircularProgressIndicator());
            final prenotations = (state).prenotations;
            return SingleChildScrollView(
              child: Row(
                children: prenotations
                    .map((e) => _otherprenotation(e, context))
                    .toList(),
              ),
            );
          }),
          Text("Le tue prenotazioni"),
          BlocBuilder<PrenotationsCubitCubit, PrenotationsCubitState>(
              builder: (context, state) {
            if (!(state is PrenotationsLoaded))
              return Center(child: CircularProgressIndicator());
            final prenotations = (state).userprenotations;
            return SingleChildScrollView(
              child: Row(
                children:
                    prenotations.map((e) => _prenotation(e, context)).toList(),
              ),
            );
          }),
          bookButton(context)
        ],
      ),
    );
  }

  Widget _otherprenotation(Prenotation prenotation, context) {
    return Container(
      child: _prenotationTile(prenotation, context),
    );
  }

  /* Widget _prenotation(Prenotation prenotation, context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: InkWell(
        child: Dismissible(
          key: Key("${prenotation.id}"),
          child: _prenotationTile(prenotation, context),
          confirmDismiss: (_) async {
            if (prenotation.prof == id) {
              BlocProvider.of<PrenotationsCubitCubit>(context)
                  .deletePrenotation(prenotation);
            }
            return false;
          },
        ),
      ),
    );
  }*/

  Widget _prenotation(Prenotation prenotation, context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: InkWell(
        child: _prenotationTile(prenotation, context),
        onLongPress: () {
          showDialog(
              context: context,
              builder: (_) => deleteDialog(prenotation, context));
        },
      ),
    );
  }

  Widget bookButton(context) {
    return ElevatedButton(
      child: Text("Prenota aula"),
      onPressed: () {
        print("PRESSED");
        Navigator.pushNamed(context, ADD_PRENOTATION);
      },
    );
  }

  Widget deleteDialog(Prenotation prenotation, context) {
    return AlertDialog(
      content: Form(
          child: TextButton(
        child: Text("Elimina"),
        onPressed: () async {
          BlocProvider.of<PrenotationsCubitCubit>(context)
              .deletePrenotation(prenotation);
          Navigator.pop(context);
        },
      )),
    );
  }

  Widget _prenotationTile(Prenotation prenotation, context) {
    return Container(
      width: MediaQuery.of(context).size.width / 4,
      padding: EdgeInsets.all(15.0),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(color: Colors.red),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(prenotation.room),
          Text(prenotation.prof.toString()),
          Text(prenotation.date)
        ],
      ),
    );
  }
}
