import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prenotazione_app/costants/strings.dart';
import 'package:prenotazione_app/cubit/cubit/add_prenotation_cubit.dart';
import 'package:prenotazione_app/cubit/prenotations_cubit_cubit.dart';
import 'package:prenotazione_app/data/network_service.dart';
import 'package:prenotazione_app/data/repository.dart';
import 'package:prenotazione_app/presentation/screens/add_prenotation_screen.dart';

import 'package:prenotazione_app/presentation/screens/prenotations_screen.dart';

class AppRouter {
  late Repository repository;
  late PrenotationsCubitCubit prenotationsCubitCubit;

  AppRouter() {
    repository = new Repository(networkService: new NetworkService());
    prenotationsCubitCubit = new PrenotationsCubitCubit(repository: repository);
  }
  Route generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case "/":
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                create: (BuildContext context) =>
                    PrenotationsCubitCubit(repository: repository),
                child: PrenotationsScreen(
                  id: 2,
                )));

      case ADD_PRENOTATION:
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (BuildContext context) => AddPrenotationCubit(
                      repository: repository,
                      prenotationsCubit: prenotationsCubitCubit),
                  child: AddPrenotationScreen(),
                ));

      default:
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                create: (BuildContext context) =>
                    PrenotationsCubitCubit(repository: repository),
                child: PrenotationsScreen(
                  id: 2,
                )));
    }
  }
}
