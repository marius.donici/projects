part of 'add_prenotation_cubit.dart';

@immutable
abstract class AddPrenotationState {}

class AddPrenotationInitial extends AddPrenotationState {}

class AddPrenotationError extends AddPrenotationState {
  final String error;

  AddPrenotationError(this.error);
}

class AddingPrenotation extends AddPrenotationState {}

class PrenotationAdded extends AddPrenotationState {}
