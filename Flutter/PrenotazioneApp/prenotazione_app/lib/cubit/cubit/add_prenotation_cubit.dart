import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:prenotazione_app/cubit/prenotations_cubit_cubit.dart';
import 'package:prenotazione_app/data/repository.dart';

part 'add_prenotation_state.dart';

class AddPrenotationCubit extends Cubit<AddPrenotationState> {
  final Repository repository;
  final PrenotationsCubitCubit prenotationsCubit;

  AddPrenotationCubit(
      {required this.repository, required this.prenotationsCubit})
      : super(AddPrenotationInitial());
  void addPrenotation(String room, int prof, int creator, String date) {
    if (room.isEmpty) {
      emit(AddPrenotationError("Prenotation room is empty"));
      return;
    }

    emit(AddingPrenotation());
    Timer(Duration(seconds: 2), () {
      repository.addPrenotation(room, prof, creator, date).then((prenotation) {
        prenotationsCubit.addPrenotation(prenotation);
        emit(PrenotationAdded());
      });
    });
  }
}
