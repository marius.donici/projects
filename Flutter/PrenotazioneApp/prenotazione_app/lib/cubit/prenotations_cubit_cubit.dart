import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:prenotazione_app/data/models/prenotation.dart';

import 'package:prenotazione_app/data/repository.dart';

part 'prenotations_cubit_state.dart';

class PrenotationsCubitCubit extends Cubit<PrenotationsCubitState> {
  final Repository repository;
  PrenotationsCubitCubit({required this.repository})
      : super(PrenotationsCubitInitial());

  void fetchUserPrenotations(int id) async {
    List<Prenotation> ret = [];
    List<Prenotation> otherprenotations = [];

    List<Prenotation> prenotations = await repository.fetchPrenotations();
    for (Prenotation p in prenotations) {
      if (p.prof == id) {
        ret.add(p);
      } else {
        otherprenotations.add(p);
      }
    }
    emit(PrenotationsLoaded(otherprenotations, ret));
  }

  void addPrenotation(Prenotation prenotation) {
    final currentState = state;
    if (currentState is PrenotationsLoaded) {
      final prenotationsList = currentState.userprenotations;
      prenotationsList.add(prenotation);
      repository.addPrenotation(prenotation.room, prenotation.prof,
          prenotation.creator, prenotation.date);
      emit(PrenotationsLoaded(currentState.prenotations, prenotationsList));
    }
  }

  void deletePrenotation(Prenotation prenotation) {
    final currentState = state;
    if (currentState is PrenotationsLoaded) {
      final prenotationsList = currentState.userprenotations
          .where((element) => element.id != prenotation.id)
          .toList();
      repository.deletePrenotation(prenotation.id);
      emit(PrenotationsLoaded(currentState.prenotations, prenotationsList));
    }
  }
}
