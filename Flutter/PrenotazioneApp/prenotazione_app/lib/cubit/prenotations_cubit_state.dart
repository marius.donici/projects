part of 'prenotations_cubit_cubit.dart';

@immutable
abstract class PrenotationsCubitState {}

class PrenotationsCubitInitial extends PrenotationsCubitState {}

class PrenotationsLoaded extends PrenotationsCubitState {
  final List<Prenotation> prenotations;
  final List<Prenotation> userprenotations;

  PrenotationsLoaded(this.prenotations, this.userprenotations);
}
