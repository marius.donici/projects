import 'package:flutter/material.dart';
import 'package:prenotazione_app/presentation/router.dart';

void main() {
  runApp(App(router: AppRouter()));
}

class App extends StatelessWidget {
  final AppRouter router;

  const App({Key? key, required this.router}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: router.generateRoute,
    );
  }
}
