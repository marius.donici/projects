import 'package:floor/floor.dart';
import 'package:prenotazione_app/data/models/prenotation.dart';

@dao
abstract class PrenotationDao {
  @Query('SELECT * FROM Prenotation WHERE creator = :id')
  Future<List<Prenotation>> getUserPrenotations(int id);

  @Query('SELECT * FROM Prenotations')
  Future<List<Prenotation>> getPrenotations();

  @insert
  Future<void> insertPrenotation(Prenotation prenotation);

  @delete
  Future<void> deletePrenotation(Prenotation prenotation);

  @delete
  Future<void> deletePrenotations(List<Prenotation> prenotations);
}
