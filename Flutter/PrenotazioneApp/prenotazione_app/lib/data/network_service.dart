import 'dart:convert';

import 'package:http/http.dart';

class NetworkService {
  final baseUrl = "http://10.0.2.2:3000";

  Future<List<dynamic>> fetchPrenotations() async {
    final response = await get(Uri.parse(baseUrl + "/prenotations"));
    return jsonDecode(response.body) as List;
  }

  Future<Map<String, dynamic>> addPrenotation(
      Map<String, dynamic> prenotationObj) async {
    print("AAA");
    final response = await post(Uri.parse(baseUrl + "/prenotations"),
        body: json.encode(prenotationObj));
    print(response.body);
    return jsonDecode(response.body);
  }

  deletePrenotation(int id) async {
    try {
      await delete(Uri.parse(baseUrl + "/prenotations/$id"));
      return true;
    } catch (er) {
      return false;
    }
  }
}
