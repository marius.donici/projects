import 'dart:convert';

import 'package:floor/floor.dart';

@entity
class Prenotation {
  @primaryKey
  int id;
  String room;
  int prof;
  int creator;
  String date;
  Prenotation({
    required this.id,
    required this.room,
    required this.prof,
    required this.creator,
    required this.date,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'room': room,
      'prof': prof,
      'creator': creator,
      'date': date,
    };
  }

  factory Prenotation.fromMap(Map<String, dynamic> map) {
    return Prenotation(
      id: map['id'],
      room: map['room'],
      prof: map['prof'],
      creator: map['creator'],
      date: map['date'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Prenotation.fromJson(Map json) {
    return Prenotation(
        id: json["id"] as int,
        room: json["room"],
        prof: json["prof"] as int,
        creator: json["creator"] as int,
        date: json["date"]);
  }
}
