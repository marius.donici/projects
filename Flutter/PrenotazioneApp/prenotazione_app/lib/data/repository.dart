import 'package:prenotazione_app/data/models/prenotation.dart';
import 'package:prenotazione_app/data/network_service.dart';

class Repository {
  final NetworkService networkService;

  Repository({required this.networkService});

  Future<List<Prenotation>> fetchPrenotations() async {
    final prenotationsRaw = await networkService.fetchPrenotations();
    return prenotationsRaw.map((e) => Prenotation.fromMap(e)).toList();
  }

  Future<Prenotation> addPrenotation(
      String room, int prof, int creator, String date) async {
    final prenotationObj = {
      "room": room,
      "prof": prof,
      "creator": creator,
      "date": date
    };

    final prenotationMap = await networkService.addPrenotation(prenotationObj);
    print(prenotationMap);
    //if (prenotationMap == null) return null;

    return Prenotation.fromJson(prenotationMap);
  }

  Future<bool> deletePrenotation(int id) async {
    return await networkService.deletePrenotation(id);
  }
}
