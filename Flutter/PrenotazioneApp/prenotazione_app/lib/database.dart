import 'package:floor/floor.dart';
import 'package:prenotazione_app/daos/prenotation_dao.dart';
import 'package:prenotazione_app/data/models/prenotation.dart';

import 'dart:async';

import 'package:sqflite/sqflite.dart' as sqflite;

part 'database.g.dart';

@Database(version: 1, entities: [Prenotation])
abstract class AppDatabase extends FloorDatabase {
  PrenotationDao get prenotationDao;
}
