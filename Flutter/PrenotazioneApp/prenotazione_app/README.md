Il progetto ha lo scopo finale di permettere ai professori della scuola di prenotare le aule. Sarà usato il BLoC pattern, json-server e Floor.
Obbiettivi da raggiungere:
- strutturare l'applicazione secondo BLoC pattern coi cubit, di cui si avrà bisogno di uno per la prenotazione, uno l'aggiunta della prenotazion e uno per l'utente
- server REST, più precisamente json-server
- database locale in Floor
- sistema login, che a seconda se l'utente sarà un professore o un admin, verrà indirizzato in una pagina diversa


Per l'applicazione è stato usato il BLoC pattern coi Cubit. Ci sono due cubit:
- PrenotationCubit con PrenotationState, gestisce l'inserimento, la cancellazione e la lettura delle prenotazioni
- AddPrenotationCubit con AddPrenotationState si occupa dell'inserimento delle prenotazioni
PrenotationCubit ha uno stato, PrenotationLoaded che viene emesso una volta che le prenotazioni sono state lette dal server.
AddPrenotationCubit ha come stati AddPrenotationError in caso di errore, AddingPrenotation quando una prenotazioni viene inserita e PrenotationAdded se la prenotazione è stata inserita.
Inoltre abbiamo altre due classi:
- NetworkService che si occupa delle richieste al server
- Repository che invoca i metodi del NetworkService e avrebbe dovuto gestire anche le query al database locale
Le prenotazioni sono salvate su json-server, dove sarebbe dovute esserci anche gli utenti.
E' stato scelto json-server perchè molto facile da usare e perchè è stato usato anche per progetti precedenti quindi si sapeva che problemi ci potrebbero esserci, di conseguenza ha accelerato il lavoro.

Il principale metodo del AddPrenotationCubit è:


```dart
  AddPrenotationCubit(
      {required this.repository, required this.prenotationsCubit})
      : super(AddPrenotationInitial());
  void addPrenotation(String room, int prof, int creator, String date) {
    if (room.isEmpty) {
      emit(AddPrenotationError("Prenotation room is empty"));
      return;
    }
   emit(AddingPrenotation());
    Timer(Duration(seconds: 2), () {
      repository.addPrenotation(room, prof, creator, date).then((prenotation) {
        prenotationsCubit.addPrenotation(prenotation);
        emit(PrenotationAdded());
      });
    });
  }

```
  

  Il metodo del PrenotationCubit che legge le prenotazioni :
```dart
    void fetchUserPrenotations(int id) async {
    List<Prenotation> ret = [];
    List<Prenotation> otherprenotations = [];

    List<Prenotation> prenotations = await repository.fetchPrenotations();
    for (Prenotation p in prenotations) {
      if (p.prof == id) {
        ret.add(p);
      } else {
        otherprenotations.add(p);
      }
    }
    emit(PrenotationsLoaded(otherprenotations, ret));
  }
```

  Gli altri metodi funzionano in modo simile.

  Dall'interfaccia delle prenotazioni i metodi di PrenotationCubit vengono invocati in questo modo:
```dart

  BlocProvider.of<PrenotationsCubitCubit>(context).fetchUserPrenotations(id);
  BlocProvider.of<PrenotationsCubitCubit>(context).deletePrenotation(prenotation);
```

  Nel AddPrenotationScreen abbiamo un listener per quando un prenotazione viene aggiunta :
```dart

  BlocListener<AddPrenotationCubit, AddPrenotationState>(
        listener: (context, state) {
          if (state is PrenotationAdded) {
            Navigator.pop(context);
          }
        }
```

Obbiettivi non raggiungiunti :
- database locale in Floor
- sistema login



