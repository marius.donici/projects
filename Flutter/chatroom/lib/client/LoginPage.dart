import 'dart:convert';
import 'dart:ui';
import 'Chatroom.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main(List<String> args) {
  runApp(LoginPage());
}

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: MyLoginPage(),
      ),
    );
  }
}

class MyLoginPage extends StatefulWidget {
  _MyLoginPageState createState() => _MyLoginPageState();
}

class _MyLoginPageState extends State<MyLoginPage> {
  TextEditingController utente = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/background.jpeg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'images/aripasx.png',
                    height: 120.0,
                    width: 120.0,
                  ),
                  Text(
                    "FreeChat",
                    style: TextStyle(
                      color: Color.fromRGBO(18, 130, 162, 1.0),
                      fontSize: 40.0,
                      fontFamily: 'BigShouldersStencilText',
                    ),
                  ),
                  Image.asset(
                    'images/aripadx.png',
                    height: 120.0,
                    width: 120.0,
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 100,
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 300,
                    height: 100,
                    child: TextFormField(
                      controller: utente,
                      style: TextStyle(
                        color: Color.fromRGBO(18, 130, 162, 1.0),
                      ),
                      cursorColor: Color.fromRGBO(18, 130, 162, 1.0),
                      cursorWidth: 5.0,
                      decoration: InputDecoration(
                        labelText: "Utente",
                        labelStyle: TextStyle(
                          color: Color.fromRGBO(18, 130, 162, 1.0),
                          fontSize: 20.0,
                        ),
                        helperText:
                            "gli altri ti riconoscerano con il nome inserito",
                        helperStyle: TextStyle(
                          color: Color.fromRGBO(18, 130, 162, 1.0),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 50,
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: FlatButton(
                      onPressed: () => formHandle(),
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                          color: Color.fromRGBO(18, 130, 162, 1.0),
                          width: 2,
                        ),
                      ),
                      child: Text(
                        "Connetti",
                        style: TextStyle(
                          fontSize: 25.0,
                          color: Color.fromRGBO(18, 130, 162, 1.0),
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }

  void formHandle() async {
    //preparo l'username da mandare al server
    Map payload = {"utente": utente.text};
    // ignore: unused_local_variable
    var response = await http.post("http://192.168.178.29:30001/",
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
        body: JsonEncoder().convert(payload));

    var message = response.body;
    print(message);
    //se il server risponde positivamente entro nella chatroom
    if (message == "OK") {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => Chatroom(utente.text)));
    } else {
      print("Username già in uso");
    }
  }
}
