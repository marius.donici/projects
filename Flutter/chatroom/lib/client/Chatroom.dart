import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';

// ignore: unused_element
WebSocket _ws;
List<Message> _messages;
String utente;

class Chatroom extends StatelessWidget {
  Chatroom(String u) {
    utente = u;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Chatroom",
          style: TextStyle(
            color: Color.fromRGBO(18, 130, 162, 1.0),
            fontSize: 30.0,
            fontFamily: 'NerkoOne-Regular',
          ),
          textAlign: TextAlign.center,
        ),
        backgroundColor: Color.fromRGBO(10, 17, 40, 1.0),
      ),
      body: MyChatroom(),
    ));
  }
}

// ignore: must_be_immutable
class Message extends StatelessWidget {
  String username;
  String content;

  Message(String name, String text) {
    username = name;
    content = text;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
              child: Text(
            "$username",
            style: TextStyle(
              color: Color.fromRGBO(253, 140, 4, 1.0),
              fontSize: 20.0,
              fontFamily: 'NerkoOne-Regular',
            ),
          )),
          Container(
              child: Text(
            "$content",
            style: TextStyle(
                color: Color.fromRGBO(18, 130, 162, 1.0), fontSize: 15.0),
          )),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}

class MyChatroom extends StatefulWidget {
  @override
  _MyChatroomState createState() => _MyChatroomState();
}

class _MyChatroomState extends State<MyChatroom> {
  TextEditingController msg = TextEditingController();

  _MyChatroomState() {
    _messages = new List();
    //connessione al server
    make();
  }
  void onMessage(data) {
    String user;
    String txt;
    //estrago il nome del mittente e il suo messaggio
    user = data.toString().split('|').first;
    txt = data.toString().split('|').last;
    setState(() {
      _messages.add(new Message(user, txt));
    });
  }

  void make() async {
    _ws = await WebSocket.connect("ws://192.168.178.29:30001");
    _ws.add("Mi sono connesso alla chatroom");
    _ws.listen(onMessage);
  }

  final _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    Timer(
      Duration(microseconds: 100),
      () => _controller.jumpTo(_controller.position.maxScrollExtent),
    );
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Container(
          color: Color.fromRGBO(0, 31, 84, 1.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  height: (height / 100) * 77,
                  width: (width / 100) * 70,
                  child: new ListView.builder(
                      controller: _controller,
                      itemCount: _messages.length,
                      itemBuilder: (BuildContext context, int index) {
                        return new Message(_messages[index].username,
                            _messages[index].content);
                      })),
              Container(
                height: (height / 100) * 10,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: (width / 100) * 2.5,
                    ),
                    Container(
                      height: (height / 100) * 10,
                      width: (width / 100) * 75,
                      child: TextFormField(
                        showCursor: true,
                        controller: msg,
                        style: TextStyle(
                          color: Color.fromRGBO(253, 140, 4, 1.0),
                        ),
                        cursorColor: Color.fromRGBO(253, 140, 4, 1.0),
                        cursorWidth: 5.0,
                        decoration: InputDecoration(
                          labelText: "Scrivi un messaggio...",
                          labelStyle: TextStyle(
                            color: Color.fromRGBO(253, 140, 4, 0.5),
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: (width / 100) * 5,
                    ),
                    Container(
                      height: (height / 100) * 10,
                      width: (width / 100) * 15,
                      child: FloatingActionButton(
                        onPressed: () => sendMessage(),
                        child: Icon(Icons.send),
                        backgroundColor: Color.fromRGBO(253, 140, 4, 0.8),
                        //foregroundColor: Color.fromRGBO(10, 17, 40, 1.0),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }

  void sendMessage() {
    _ws.add(msg.text);
    setState(() {
      _messages.add(new Message("Tu", msg.text));
    });
    msg.clear();
  }
}
