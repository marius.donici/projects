import 'dart:convert';
import 'dart:io';

List<Client> clients = new List();
List<String> utenti = new List();

InternetAddress ip;

void main() async {
  // ignore: unused_local_variable

  var server = await HttpServer.bind("192.168.178.29", 30001);

  print("${server.address}");
  print("${server.port}");

  await for (HttpRequest request in server) {
    //post per il login
    if (request.method == 'POST') {
      //estrago il nome utente dalla richiesta
      var s = await utf8.decoder.bind(request).join();
      // ignore: await_only_futures
      var val = await s;
      //controllo e aggiunta dell'utente
      if (val.contains("utente")) {
        val = val.substring(11, val.length - 2);
        if (!utenti.contains(val)) {
          utenti.add(val);
          clients.add(new Client(request.connectionInfo.remoteAddress, val));
          print("aggiunto utente : " + val);
          request.response.write("OK");
          request.response.close();
        }
      }
      //viene ricevuto un messaggio
    } else if (WebSocketTransformer.isUpgradeRequest(request)) {
      ip = request.connectionInfo.remoteAddress;

      WebSocketTransformer.upgrade(request).then(handleWebSocket);
    } else {
      print("Connessione non riconosciuta");
    }
  }
}

bool ipInServer(InternetAddress ip) {
  for (Client c in clients) {
    if (c.getipAddress() == ip) {
      return true;
    }
  }
  return false;
}

String getUserByIp(InternetAddress ip) {
  for (Client c in clients) {
    if (c.ipAddress == ip) {
      return c.username;
    }
  }
  return null;
}

void handleWebSocket(WebSocket socket) {
  String user = getUserByIp(ip);
  //verifica se il client è per la prima volta nella chatroom
  if (!ipInServer(ip)) {
    clients.last.setWebSocket(socket);
  } else {
    socket.listen((data) {
      String msg = data.toString();
      for (Client c in clients) {
        if (c.ipAddress != ip) {
          c.socket.add("$user | $msg");
        }
      }
    });
  }
}

class Client {
  WebSocket socket;
  InternetAddress ipAddress;
  String username;

  Client(InternetAddress ip, String user) {
    ipAddress = ip;
    username = user;
  }

  String getusername() {
    return username;
  }

  InternetAddress getipAddress() {
    return ipAddress;
  }

  void setWebSocket(WebSocket s) {
    socket = s;
  }
}
