# chatroom

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


Il server si mette in ascolto, e a seconda del tipo di richiesta risponde in maniera diversa. Se la richiesta è POST vuol dire che l'utente sta entrando nella chatroom e si è registrato con un nome utente. Il nome utente viene controllato se è gia inserito, e poi viene creato un oggetto Client, con il suo nome utente e l'indirizzo IP. Quando un client entra nella chatroom, manda un primo messaggio al server, in questo modo il server crea subito il socket e lo inserisce nell'oggetto Client. Dopo aver ricevuto il messaggio, il server manda una stringa che contiene il nome del miittente e il suo messaggio a tutti gli altri client connessi escludendo il mittente.
La Chatroom è dotata di una ListView, in cui vengono aggiunti e visualizzati i messaggi ricevuti, di una TextFormField, che serve per scrivere il messaggio e un FloatingActionButton per mandare il messaggio.
