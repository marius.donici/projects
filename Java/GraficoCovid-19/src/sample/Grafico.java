package sample;

import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

public class Grafico {

    private String reg;
    private boolean[] flag;
    private Covid[] covids;
    private LineChart lc;

    public Grafico (String reg, boolean[] flag, Covid[] covids) {

        this.reg = reg;
        this.flag = flag;
        this.covids = covids;
        createLineChart();

    }

    private void createLineChart() {

        CategoryAxis x = new CategoryAxis();
        NumberAxis y = new NumberAxis();

        XYChart.Series totale = new XYChart.Series();
        XYChart.Series deceduti = new XYChart.Series();
        XYChart.Series guariti = new XYChart.Series();

        for (Covid covid : covids) {
            if (covid.getRegione().equals(this.reg)) {
                if (flag[0]) {
                    totale.getData().add(new XYChart.Data(covid.getData(),covid.getTotale()));
                }

                if (flag[1]) {
                    guariti.getData().add(new XYChart.Data(covid.getData(),covid.getDimessi_guariti()));
                }

                if (flag[2]) {
                    deceduti.getData().add(new XYChart.Data(covid.getData(),covid.getDeceduti()));
                }
            }
        }

        lc = new LineChart(x,y);
        lc.getData().add(totale);
        lc.getData().add(deceduti);
        lc.getData().add(guariti);
    }

    public LineChart getLC() {
        return lc;
    }
}
