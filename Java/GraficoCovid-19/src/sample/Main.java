package sample;


import com.google.gson.Gson;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.EventHandler;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

public class Main extends Application {

    Covid[] covids;
    ComboBox regioniBox;
    ObservableList<String> date;
    ObservableList<String> regioni;

    BorderPane root;

    LineChart lc;

    String reg;
    boolean[] flag = new boolean[3];

    @Override
    public void start(Stage primaryStage) throws Exception{
        Gson gson = new Gson();


        //scarico i dati
        try(Reader r = new InputStreamReader(new URL("https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json").openStream(),"UTF-8")) {
            covids = gson.fromJson(r, Covid[].class);

        }catch (Exception e) {
            e.printStackTrace();
        }

        //dati la combobox
        regioni = FXCollections.observableArrayList();

        uploadData();



         regioniBox= new ComboBox(regioni);
         regioniBox.setOnAction((event) -> {
             reg = regioniBox.getSelectionModel().getSelectedItem().toString();
             System.out.println(reg);
             //crea grafico
             Grafico g = new Grafico(reg, flag, covids);
             lc = g.getLC();
             root.setCenter(lc);
         });
        CheckBox  totaleCB = new CheckBox("totale_casi");
        CheckBox guaritiCB = new CheckBox("dimessi_guariti");
        CheckBox decedutiCB = new CheckBox("deceduti");










        TilePane t = new TilePane();
        t.getChildren().add(totaleCB);
        t.getChildren().add(guaritiCB);
        t.getChildren().add(decedutiCB);


        root = new BorderPane();
        root.setTop(new HBox(regioniBox));
        root.setLeft(t);


        decedutiCB.setOnAction(e -> handleOptions(decedutiCB, totaleCB, guaritiCB));
        guaritiCB.setOnAction(e -> handleOptions(decedutiCB, totaleCB, guaritiCB));
        totaleCB.setOnAction(e -> handleOptions(decedutiCB, totaleCB, guaritiCB));







        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }

    private void uploadData() {
        new Thread(){
            @Override
            public void run() {
                for (Covid covid : covids){
                    if (!regioni.contains(covid.getRegione())){
                        regioni.add(covid.getRegione());
                    }

                }
            }
        }.start();
    }

    private void handleOptions(CheckBox box1, CheckBox box2, CheckBox box3) {

        if (box1.isSelected()) {
            flag[2] = true;
        } else if (!box1.isSelected()) {
            flag[2] = false;
        }

        if (box2.isSelected()) {
            flag[0] = true;
        } else if(!box2.isSelected()) {
            flag[0] = false;
        }

        if (box3.isSelected()) {
            flag[1] = true;
        } else if (!box3.isSelected()){
            flag[1] = false;

        }

        //crea grafico
        Grafico g = new Grafico(reg, flag, covids);
        lc = g.getLC();
        root.setCenter(lc);

    }




    public static void main(String[] args) {
        launch(args);
    }
}


