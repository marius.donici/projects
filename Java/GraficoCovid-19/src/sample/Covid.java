package sample;

public class Covid {

    private String data;
    private int nuovi_positivi;
    private int totale_casi;
    private int codice_regione;
    private int deceduti;
    private int dimessi_guariti;
    private String denominazione_regione;

    public String toString() {
        StringBuilder ret = new StringBuilder();

        ret.append("REGIONE : ").append(denominazione_regione).append('\n');
        ret.append("DATA : ").append(data).append('\n');

        ret.append("TOTALE : ").append(totale_casi).append('\n');
        ret.append("NUOVI POSITIVI : ").append(nuovi_positivi).append('\n');
        ret.append("DIMESSI GUARITI : ").append(dimessi_guariti).append('\n');


        return ret.toString();
    }

    public int getCodice_regione() {
        return this.codice_regione;
    }

    public String getRegione() {
        return this.denominazione_regione;
    }

    public String getData() {
        return this.data;
    }

    public int getTotale () {
        return this.totale_casi;
    }

    public int getDimessi_guariti() {
        return this.dimessi_guariti;
    }

    public int getDeceduti() {
        return this.deceduti;
    }

}
