/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graficofunzione;

/**
 *
 * @author marius
 */
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GraficoFunzione extends Application{
        @Override
    public void start(Stage primaryStage) throws Exception{


        Map<Double, Double> p = new HashMap<>();

        Scanner sc = new Scanner(System.in);

        System.out.println("Inserisci a: ");
        double a = sc.nextDouble();

        System.out.println("Inserisci b: ");
        double b = sc.nextDouble();

        System.out.println("Inserisci n: ");
        int n = sc.nextInt();
              double x = a;
        double delta = (b-a)/(n-1);
        int i = 1;
        
        double min = f(x);
        double max = f(x);

        while (x <= b) {

            double y = f(x);

            if (y > max) {
                max = y;
            }

            if (y < min) {
                min = y;
            }

            p.put(x,y);

            x = i * delta;
            i++;

        }


        final NumberAxis xAsis = new NumberAxis(a, b, a+b);
        final NumberAxis yAsis = new NumberAxis(min, max, min+max);
        final LineChart<Number, Number> lc = new LineChart<Number, Number>(xAsis, yAsis);
        lc.setTitle("Line  Chart");
        xAsis.setLabel("X");
        yAsis.setLabel("Y");

        XYChart.Series series1 = new XYChart.Series();
        series1.setName("---------");



        for (Map.Entry<Double, Double> entry : p.entrySet()) {
            series1.getData().add(new XYChart.Data(entry.getKey(), entry.getValue()));
        }





        primaryStage.setTitle("Grafico");
        StackPane root = new StackPane();
        root.getChildren().add(lc);
        lc.getData().addAll(series1);
        Scene scene = new Scene(root, 1800, 800);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
 static double f(double x) {
        double ret = (Math.pow(2, -x/10))*Math.sin(x);
        return ret;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        launch(args);
    }
    
}
