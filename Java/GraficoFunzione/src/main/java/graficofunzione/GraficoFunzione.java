package graficofunzione;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Stefano Giacomello
 */
public class GraficoFunzione extends Application {

  @Override
  public void start(Stage primaryStage) {

    final NumberAxis xAxis = new NumberAxis(-5, 10, 1);
    final NumberAxis yAxis = new NumberAxis(-100, 500, 100);
    final LineChart<Number, Number> lc = new LineChart<Number, Number>(xAxis, yAxis);
    lc.setTitle("Line Chart");
    xAxis.setLabel("X");
    yAxis.setLabel("Y");

    XYChart.Series series1 = new XYChart.Series();
    series1.setName("----");
    series1.getData().add(new XYChart.Data(-4.2, 193.2));
    series1.getData().add(new XYChart.Data(-5.8, 33.6));
    series1.getData().add(new XYChart.Data(-2.2, 24.8));
    series1.getData().add(new XYChart.Data(1, 14));
    series1.getData().add(new XYChart.Data(1.2, 26.4));
    series1.getData().add(new XYChart.Data(4.4, 114.4));
    series1.getData().add(new XYChart.Data(8.5, 323));

    primaryStage.setTitle("Grafico a linee");
    StackPane root = new StackPane();
    root.getChildren().add(lc);
    lc.getData().addAll(series1);
    Scene scene = new Scene(root, 500, 400);
    primaryStage.setScene(scene);
    primaryStage.show();

  }

  public static void main(String[] args) {
    launch(args);
  }

}
