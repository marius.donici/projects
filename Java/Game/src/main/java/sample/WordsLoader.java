package sample;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class WordsLoader extends Service<ArrayList<String>> {
    private ArrayList<String> words;
    @Override
    protected Task<ArrayList<String>> createTask() {
        words = new ArrayList<>();
        return new Task<ArrayList<String>>() {
            @Override
            protected ArrayList<String> call() throws Exception {
                System.out.println(getClass().getClassLoader().getResource("words.txt").getPath());
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(getClass().getClassLoader().getResource("words.txt").getPath()), StandardCharsets.UTF_8));
                    //BufferedReader reader = new BufferedReader(new FileReader(getClass().getClassLoader().getResource("words.txt").getPath()));
                    String line = reader.readLine();
                    while (line != null) {
                        words.add(line);
                        line = reader.readLine();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return words;
            }
        };
    }

    public ArrayList<String> getWords() {
        return this.words;
    }
}
