package sample;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import java.util.ArrayList;
import java.util.Random;

public class Controller {

    private ArrayList<String> words = new ArrayList<>();

    @FXML
    private ListView<String> listView;
    @FXML
    private Button goBtn;

    @FXML
    private Button[][] matrix;

    @FXML
    private Button btn00, btn01, btn02, btn03, btn10, btn11, btn12, btn13, btn20, btn21, btn22, btn23, btn30, btn31, btn32, btn33;



    private boolean isClicked[][] = new boolean[4][4];

    private ArrayList<Button> clicked = new ArrayList<>();

    private ArrayList<String> word = new ArrayList<>();


    private Random rnd = new Random();

    public void initialize() {
        //carico le parole dal file
        WordsLoader wl = new WordsLoader();
        wl.setOnSucceeded(e -> {
            words = wl.getWords();
            System.out.println(words.size());
        });
        wl.start();
    }







    @FXML
    private void btnClicked(ActionEvent event) {


        Button tmp = (Button)event.getSource();

        if (isClicked[extractFirstNumber(tmp)][extractSecondNumber(tmp)]) {
            if (tmp == clicked.get(clicked.size()-1)){
                tmp.setStyle("");
                clicked.remove(tmp);
                isClicked[extractFirstNumber(tmp)][extractSecondNumber(tmp)] = false;
                word.remove(word.size()-1);
            }else if (clicked.contains(tmp)) {
                resetAllClickedButtons();
                word.clear();
            }
        }else {
            if (canBeClicked(tmp)) {


                if (tmp.getStyle().equals("")) {

                    String color = String.format("-fx-background-color: rgb(%d,%d,%d)", rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255));
                    tmp.setStyle(color);
                    word.add(tmp.getText());
                }
            }

        }



    }

    @FXML
    private void goButton(ActionEvent event) {

        if (words.contains(toStringWord().toLowerCase())) {
            listView.getItems().add(toStringWord());
            word.clear();



        }
        resetAllClickedButtons();


    }



    private String toStringWord() {
        String ret = "";
        for (String s:word) {
            ret += s;
        }
        return ret;
    }



    private void resetAllClickedButtons() {
        for (Button b : clicked) {
            b.setStyle("");

            isClicked[extractFirstNumber(b)][extractSecondNumber(b)] = false;
        }

        clicked.clear();

    }

    private boolean canBeClicked(Button btn) {

        //il primo e il secondo numero del nome del bottone sono le sue coordinate



        int firstNumber = extractFirstNumber(btn);
        int secondNumber = extractSecondNumber(btn);

        System.out.println(firstNumber + " " + secondNumber);




        if (clicked.isEmpty()) {
            clicked.add(btn);
            isClicked[firstNumber][secondNumber] = true;

            return true;
        }

        int up = Math.abs(firstNumber-1);
        int down = Math.abs(firstNumber+1);
        int right = Math.abs(secondNumber+1);
        int left = Math.abs(secondNumber-1);


        if (down == 4 ) {
            down = 3;
        }
        if (right == 4) {
            right = 3;
        }



        if (isClicked[firstNumber][left] || isClicked[firstNumber][right]) { //destra o sinistra clickato
            clicked.add(btn);
            isClicked[firstNumber][secondNumber] = true;

            return true;
        } else if(isClicked[up][secondNumber] || isClicked[down][secondNumber]) { //sopra o sotto clickato
            clicked.add(btn);
            isClicked[firstNumber][secondNumber] = true;

            return true;
        } else {
            //diagonale
            if (isClicked[up][right] || isClicked[down][right] || isClicked[up][left] || isClicked[up][down]) {
                clicked.add(btn);
                isClicked[firstNumber][secondNumber] = true;
                return true;
            }

        }

        return false;




    }

    private int extractFirstNumber(Button btn) {
        String id = btn.getId();

        return Integer.parseInt(String.valueOf(id.charAt(3)));
    }

    private int extractSecondNumber(Button btn) {
        String id = btn.getId();
        return Integer.parseInt(String.valueOf(id.charAt(4)));
    }







}
