package sample;

import javafx.application.Application;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;


public class Main extends Application {

    static FXMLLoader loader;
    private Button goBtn;

    private ArrayList<String> words = new ArrayList<>();

    static Stage primaryStage;







    @Override
    public void start(Stage stage) throws Exception{
        primaryStage = stage;







        loader = new FXMLLoader(getClass().getClassLoader().getResource("sample.fxml"));
        Parent root = loader.load();

        disableButtons();

        MenuBar menuBar = (MenuBar) loader.getNamespace().get("menuBar");
        MenuItem newGame = new MenuItem("New Game");
        MenuItem stopGame = new MenuItem("Stop Game");
        stopGame.setOnAction(e -> {
            System.out.println("stop game");
            disableButtons();

        });
        newGame.setOnAction(e -> {
            System.out.println("new game");
            startGame();
        });
        menuBar.getMenus().get(0).getItems().add(newGame);
        menuBar.getMenus().get(0).getItems().add(stopGame);


        primaryStage.setTitle("Game");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }

    private void disableButtons() {
        goBtn = (Button) loader.getNamespace().get("goBtn");
        goBtn.setDisable(true);

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                Button btn = (Button) loader.getNamespace().get("btn"+i+""+j);
                btn.setText("?");
                btn.setDisable(true);

            }

        }
    }

    private void enableButtons() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                double random = 0 + (new Random().nextDouble() * 2.0);
                Button btn = (Button) loader.getNamespace().get("btn"+i+""+j);

                btn.setDisable(false);
            }
        }
        goBtn.setDisable(false);
    }



    private void startGame() {

        //parte la progress bar
        ProgressBarService pbs= new ProgressBarService();

        pbs.setOnSucceeded(e -> {
            disableButtons();
            pbs.cancel();

        });
        pbs.start();

        //genero le lettere per ogni bottone
        CumulativeFrequency cf = new CumulativeFrequency();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                double random = 0 + (new Random().nextDouble() * 2.0);
                Button btn = (Button) loader.getNamespace().get("btn"+i+""+j);

                btn.setText(cf.getLetter(random));
            }
        }




        enableButtons();

    }


    public static void main(String[] args) {

        launch(args);
    }
}
