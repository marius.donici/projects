package sample;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CumulativeFrequency {

    private Map<String, Double> letterFrequencies;


    public CumulativeFrequency() {

        letterFrequencies = new HashMap<>();

        letterFrequencies.put("A", 0.8167);
        letterFrequencies.put("B", 0.1492);
        letterFrequencies.put("C", 0.2782);
        letterFrequencies.put("D", 0.4252);
        letterFrequencies.put("E", 0.12702);
        letterFrequencies.put("F", 0.2228);
        letterFrequencies.put("G", 0.2015);
        letterFrequencies.put("H", 0.6094);
        letterFrequencies.put("I", 0.6966);
        letterFrequencies.put("J", 0.0153);
        letterFrequencies.put("K", 0.0772);
        letterFrequencies.put("L", 0.4025);
        letterFrequencies.put("M", .02406);
        letterFrequencies.put("N", 0.6749);
        letterFrequencies.put("O", 0.7507);



        makeCumulativeTable();



    }

    private void makeCumulativeTable() {
        Double prec = 0.0;
        Iterator it = letterFrequencies.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            pair.setValue((Double)pair.getValue() + prec);
            prec = (Double) pair.getValue();

        }

        /*Iterator it2 = letterFrequencies.entrySet().iterator();
        while (it2.hasNext()) {
            Map.Entry pair = (Map.Entry)it2.next();
            System.out.println(pair.getKey() + " -> " + pair.getValue());

        }*/
    }

    public String getLetter(double random) {
        double prec = 0.0;

        Iterator it = letterFrequencies.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if ((Double)pair.getValue() >= random && random > prec) {
                return (String) pair.getKey();
            }
            prec = (Double)pair.getValue();


        }

        return "";
    }
}
