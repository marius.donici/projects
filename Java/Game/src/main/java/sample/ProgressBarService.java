package sample;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressBar;

import static sample.Main.loader;


public class ProgressBarService extends Service<Void> {

    ProgressBar pb = (ProgressBar) loader.getNamespace().get("progressBar");
    @Override
    protected Task<Void> createTask() {

        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                long startTime = System.currentTimeMillis() / 1000;

                do{
                    long endTime = System.currentTimeMillis() / 1000;

                    pb.setProgress((double) (endTime-startTime) / 30);

                } while (((System.currentTimeMillis() / 1000) - startTime) <= 30);
                return null;
            }
        };


    }
}
