package sample;

import com.google.gson.Gson;
import javafx.application.Application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.scene.Scene;

import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;

import java.util.Map;
import java.util.TreeMap;

public class Main extends Application {

    private TreeMap<String, String> map;
    private ObservableList<String> list;


    @Override
    public void start(Stage primaryStage) throws Exception{

        BorderPane root = new BorderPane();



        map = new TreeMap<>();
        list = FXCollections.observableArrayList();



        BufferedReader br = new BufferedReader(new FileReader(getClass().getClassLoader().getResource("dictionary.json").getFile()));
        Gson gson = new Gson();

        Dizionario[] dizionario = gson.fromJson(br, Dizionario[].class);



        for (Dizionario d : dizionario) {
            list.add(d.getEntry());
            map.put(d.getEntry(), d.getDefinition());

        }




        ListView<String> listView = new ListView<>();
        listView.setItems(list);

        listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                root.setCenter(new TextArea(map.get(listView.getSelectionModel().getSelectedItem())));

            }
        });

        TextField filter = new TextField();

        root.setBottom(new Label("Parole trovate : " + list.size()));

        filter.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                if (filter.getText().equals("")) {
                    listView.setItems(list);
                    root.setBottom(new Label("Parole trovate : " + list.size()));
                } else {

                    ObservableList<String> filteredList = FXCollections.observableArrayList();


                    for (Map.Entry<String, String> entry : map.entrySet()) {
                        if (entry.getKey().contains(filter.getText())) {
                            filteredList.add(entry.getKey());


                        }
                    }

                    listView.setItems(filteredList);
                    root.setBottom(new Label("Parole trovate : " + filteredList.size()));
                }

                listView.refresh();



            }
        });





        root.setTop(filter);



        root.setLeft(listView);

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 800, 800));
        primaryStage.show();
    }

    private void applyFilter(String filter) {

        for(Map.Entry<String, String> entry: map.entrySet()) {
            if (entry.getKey().contains(filter)) {
                System.out.println(entry.getKey());


            }
        }

    }


    public static void main(String[] args) {
        launch(args);
    }
}
