package sample;

public class MyTreeMap<K extends Comparable<K>, V> {

    private class Node {
        private K key;
        private V value;
        Node left;
        Node right;

        public Node (K key, V value) {
            this.key = key;
            this.value = value;
        }


    }

    private Node root;
    private int size;





    public Node put(K key, V value) {
        return root =put(key,value, root);
    }

    public V get(K key) {
        return get(root, key);
    }

    private V get (Node n, K key) {
        if (n == null) {
            return null;
        } else if (key.compareTo(n.key) < 0) {
            return get(n.left, key);
        } else if (key.compareTo(n.key) > 0) {
            return get(n.right, key);
        } else {
            return n.value;
        }
    }

    private Node put (K key, V value, Node n) {
        if (n == null) {
            n = new Node(key, value);
        } else if (key.compareTo(n.key) < 0) {
            n.left = put(key, value, n.left);
        } else if (key.compareTo(n.key) > 0) {
            n.right = put(key, value, n.right);
        } else {
            n.value = value;
        }
        return n;
    }


}
