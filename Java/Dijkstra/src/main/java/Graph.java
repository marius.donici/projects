import java.util.LinkedList;
import java.util.Queue;

public class Graph {

    private int matrix[][];
    private int edges;

    private boolean visited[];



    public Graph(int n) {
        matrix = new int[n][n];
        this.edges  = 0;

    }

    public void setEdge(int from, int to, int weight) {
        this.matrix[from][to] = weight;
        this.edges++;
    }

    public int getNumberOfEdges() {
        return this.edges;
    }

    public int getNumberOfNodes() {
        return this.matrix.length;
    }

    public void deleteEdge(int from, int to) {
        this.matrix[from][to] = 0;
        this.edges--;

    }

    public boolean isEdge(int from, int to) {
        return this.matrix[from][to] != 0;
    }

    public int getWeight(int from, int to) {
        return this.matrix[from][to];
    }

    public boolean isConnected() {


        for (int i = 0; i < this.edges; i++) {
            boolean connected = true;
            for (int j = 0; j < this.edges; j++) {
                if ((this.matrix[i][j] == 0) && (i != j)){
                    connected = false;
                }
            }
            if (connected) {
                return true;
            }
        }
        return false;

    }

    private void depthFirstSearch(int from) {

        System.out.println(from);
        visited[from] = true;

        for (int to = 0; to < this.getNumberOfNodes(); to++) {
            if (isEdge(from , to) && !visited[to]) {
                depthFirstSearch(to);
            }
        }

    }

    public void depthFirstSearch() {
        visited = new boolean[getNumberOfNodes()];
        depthFirstSearch(0);
    }

    public void DFS() {
        visited = new boolean[getNumberOfNodes()];
        DFS(0);
    }

    private void DFS(int i) {
        Queue<Integer> queue = new LinkedList<>();

        queue.add(i);
        visited[i] = true;

        while (!queue.isEmpty()) {
            int from = queue.remove();
            System.out.println(from);
            for (int to = 0; to < getNumberOfNodes(); to++) {
                if (isEdge(from, to) && !visited[to]) {
                    queue.add(to);
                    visited[to] = true;

                }
            }
        }
    }

    private int getMinimum(int distance[]) {
        int min = Integer.MAX_VALUE;
        int vertex = -1;
        for (int i = 0; i < getNumberOfNodes(); i++) {
            if (!visited[i] && min > distance[i]) {
                min = distance[i];
                vertex = i;
            }

        }

        return vertex;
    }

    public void dijkstra(int src) {
        visited = new boolean[getNumberOfNodes()];
        int distance[] = new int[getNumberOfNodes()];
        int INFINITY = Integer.MAX_VALUE;

        for (int i = 0; i < getNumberOfNodes(); i++) {
            distance[i] = INFINITY;
        }

        distance[src] = 0;

        for (int i = 0; i < getNumberOfNodes(); i++) {
            int u = getMinimum(distance);
            visited[u] = true;

            for (int v = 0; v < getNumberOfNodes(); v++) {
                if (isEdge(u,v)) {
                    if (!visited[v] && matrix[u][v] != INFINITY) {
                        int key = matrix[u][v] + distance[u];
                        if (key < distance[v]) {
                            distance[v] = key;
                        }
                    }
                }
            }


        }
        printDijkstra(src, distance);
    }

    private void printDijkstra(int src, int key[]) {
        for (int i = 0; i < getNumberOfNodes(); i++) {
            System.out.println(src + " --> "+ i+ " : " + key[i]);

        }
    }






}
