import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class GraphL {

    private int numberOfEdges;
    private List<Edge>[] edges;

    private boolean visited[];
    private List<Edge>[] tmp;

    public GraphL (int n) {

        this.numberOfEdges = n;
        edges = new ArrayList[n];

        for (int i = 0; i < edges.length; i++) {
            edges[i] = new ArrayList<>();
        }
    }

    public void setEdge (int from , int to , int weight) {
        if (isEdge(from, to)) {
            System.out.println("Not supported");
        } else {
            edges[from].add(new Edge(to, weight));
        }
    }

    public boolean isEdge(int from, int to) {
        for (Edge e : edges[from]) {
            if (e.getNode() == to) {
                return true;
            }
        }

        return false;
    }

    public void BFS() {
        visited = new boolean[getNumberOfEdges()];
        BFS(0);
    }

    private void BFS (int node) {

        System.out.println(node);
        visited[node] = true;


        for (Edge e : edges[node]) {
            if (!visited[e.getNode()]){
                BFS(e.getNode());
            }
        }

    }

    public void DFS() {
        visited = new boolean[getNumberOfEdges()];
        DFS(0);



    }

    private void DFS(int node) {

        Queue<Integer> queue = new LinkedList<>();
        queue.add(node);
        visited[node] = true;

        while (!queue.isEmpty()) {
            int from = queue.remove();
            System.out.println(from);

            for (int to = 0; to < getNumberOfEdges(); to++) {
                if (isEdge(from, to) && !visited[to]) {
                    queue.add(to);
                    visited[to] = true;
                }
            }
        }
    }

    private Edge getEdge(int from, int to) {
        return edges[from].get(to);
    }

    public int getNumberOfEdges() {
        return this.numberOfEdges;
    }
}
