import java.io.*;


public class Graph {

    private int matrix[][];
    private int vertices;



    public Graph(int vertices) {
        matrix = new int[vertices][vertices];
        this.vertices = vertices;
    }

    public void setEdge(int from, int to, int weight) {
        this.matrix[from][to] = weight;
    }

    public void deleteEdge(int from, int to) {
        this.matrix[from][to] = 0;
    }

    public boolean isEdge(int from, int to) {
        return this.matrix[from][to] != 0;
    }

    public int getWeight(int from, int to) {
        return this.matrix[from][to];
    }

    public boolean isConnected() {


        for (int i = 0; i < this.vertices; i++) {
            boolean connected = true;
            for (int j = 0; j < this.vertices; j++) {
                if ((this.matrix[i][j] == 0) && (i != j)){
                    connected = false;
                }
            }
            if (connected) {
                return true;
            }
        }
        return false;

    }

    public void print() {
        for (int i = 0; i < this.vertices; i++) {
            for (int j = 0; j < this.vertices; j++) {
                System.out.print(this.matrix[j][i] + "\t");
            }

            System.out.println();
        }
    }


    public void writeOnFile() throws IOException {
        BufferedWriter br = new BufferedWriter(new FileWriter("grafo.txt"));

        //scrivo la matrice
        for (int i = 0; i < this.vertices; i++) {
            for (int j = 0; j < this.vertices; j++) {
                br.write(String.valueOf(this.matrix[j][i]));
                br.write('\t');
            }
            br.write('\n');
        }

        br.write('\n');



        br.close();


    }

    //legge la dimensione del grafo scritto sul file
    private static int size(BufferedReader br)throws IOException, FileNotFoundException {
        int size = 0;
        int ch;
        while ((ch = br.read()) != -1) {
            if (ch == '\n') {
                return size;
            }
            if (!(ch == '\t')){
                size++;
            }


        }
        br.close();

        return size;
    }

    //legge il grafo dal file
    static void readFromFile() throws IOException, FileNotFoundException{





        BufferedReader br = new BufferedReader(new FileReader(new File("grafo.txt")));




        String st;
        while ((st = br.readLine()) != null){




            System.out.println(st);

        }
        br.close();



    }


}
