import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Arrays;

public class main {
    public static void main(String[] args) {
        Gson gson = new Gson();

        try(Reader r = new InputStreamReader(new URL("https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json").openStream(),"UTF-8")) {
            Coronavirus[] coronavirus = gson.fromJson(r, Coronavirus[].class);
            for (Coronavirus virus : coronavirus) {
                if (virus.getCodice_regione() == 20) {
                    System.out.println(virus);
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
