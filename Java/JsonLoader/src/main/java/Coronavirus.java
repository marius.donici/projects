
public class Coronavirus {

    private String data;
    private int nuovi_positivi;
    private int totale_casi;
    private int codice_regione;

    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append("DATA : ").append(data).append('\n');
        ret.append("NUOVI POSITIVI : ").append(nuovi_positivi).append('\n');
        ret.append("TOTALE : ").append(totale_casi).append('\n');

        return ret.toString();
    }

    public int getCodice_regione() {
        return codice_regione;
    }
}
