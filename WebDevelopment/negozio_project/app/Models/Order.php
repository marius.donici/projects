<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function products() {
        return $this->belongsTo(Product::class, 'product_id', 'id_product');
    }
    public function shops() {
        return $this->belongsTo(Shop::class, 'shop_id', 'id_shop');
    }
}
