<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function categories() {
        return $this->belongsTo(Category::class, 'category_id', 'id_category');
    }

    public function shops() {
        return $this->belongsTo(Shop::class, 'shop_id', 'id_shop');
    }

    public function colors() {
        return $this->belongsTo(Color::class, 'color_id', 'id_color');
    }

    public function materials() {
        return $this->belongsTo(Material::class, 'material_id', 'id_material');
    }




}
