<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;

class ProductController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function welcome() {
        $products = Product::all();
        return view('welcome', ["products" => $products]);
    }

    public function filter() {
        $products = Product::with(['categories', 'shops', 'colors', 'materials'])->get();
        $color = request("color");
        $material = request("material");
        $category = request("category");
        error_log($products);
        if (! empty ($color)){
            $products = $products->where('color_id', $color);
        }

        if (! empty ($material)){
            $products = $products->where('material_id', $material);
        }

        if (! empty ($category)){
            $products = $products->where('category_id', $category);
        }
        error_log('-----');
        
        
        
        return view('welcome', ["products" => $products]);

    }
}
