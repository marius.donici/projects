<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = Auth::id();
        $shops = DB::table('shops')
            ->where('user_id', '=', $id)
            ->get();

        $orders = Order::with('products', 'shops')->join('products', 'products.id_product', '=', 'orders.product_id')->join('shops', 'shops.id_shop', '=', 'products.shop_id')->join('users', 'users.id_user', '=', 'shops.user_id')->select('orders.*')->get();
        //var_dump($orders);
        return view('home', ["shops" => $shops, "orders" => $orders]);
    }
}

//CARBON