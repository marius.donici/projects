<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function users() {
        $users = User::all();
        return view('User.users', ['users' => $users]);
    }

    public function register() {
        return view('auth.register');
    }

    public function store() {
        if(request('password') == request('password2')){
            $user = new User();
            $user->name = request('name');
            $user->email = request('email');
            $user->password = Hash::make(request('password'));
            $user->save();
            return redirect('/');

        }else {
            return redirect('/register')->with('msg', 'Le password non coincidono');
        }

   
    }

    public function login() {
        
    }

    public function shops($id) {
        $shops = DB::table('shops')
                ->where('user_id', '=', $id)
                ->get();
        return view("User.shops", ["shops" => $shops]);
    }
}
