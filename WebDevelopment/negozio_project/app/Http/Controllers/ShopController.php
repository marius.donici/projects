<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Shop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTimeZone;

class ShopController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function shops() {
        $shops = Shop::all();
        return view("Shop.shops", ["shops" => $shops]);
    }

    public function products($id) {
        $products = Product::with(['categories', 'shops', 'colors', 'materials'])->get();
        $products = $products
                ->where('shop_id', '=', $id);
        return view("Shop.products", ["products" => $products]);
    }

    public function createshop() {
        return view('Shop.createshop');
    }

    public function saveshop() {


        $shop = new Shop();
        $shop->name = request('shop');
        $shop->user_id = Auth::id();
        $shop->amopening = request('amopening');
        $shop->amclosing = request('amclosing');
        $shop->pmopening = request('pmopening');
        $shop->pmclosing = request('pmclosing');
        $shop->save();
        return redirect('/home');

   
    }

    public function addproduct($id) {
        $product = new Product();
        $product->name = request('name');
        $product->description = request('description');
        $product->price = request('price');
        $product->material_id = request('material');
        $product->color_id = request('color');
        $product->category_id = request('category');
        $product->shop_id = $id;
        $product->save();
        return redirect('/homeshop/'.$id);

    }

    public function homeshop($id) {
        $products = Product::with(['categories', 'shops', 'colors', 'materials'])->get();
        $products = $products
        ->where('shop_id', '=', $id);
        $shop = Shop::where("id_shop", $id)->first();
        return view('Shop.homeshop', ["products" => $products, "shop"=>$shop]);
    }

    public static function is_open($id) {

        $shop = Shop::where('id_shop', $id)->first();
        $current_time =  Carbon::now(new DateTimeZone('CET'));

        $amopening = Carbon::createFromFormat('H:i', $shop->amopening, 'CET');
        $amclosing = Carbon::createFromFormat('H:i', $shop->amclosing, 'CET');
        $pmopening = Carbon::createFromFormat('H:i', $shop->pmopening, 'CET');
        $pmclosing = Carbon::createFromFormat('H:i', $shop->pmclosing, 'CET');
        if (($current_time->gt($amopening) && $current_time->lessThan($amclosing)) || ($current_time->gt($pmopening) && $current_time->lessThan($pmclosing))) {

            return true;
        } else {
            return false;
        }
    }
}
