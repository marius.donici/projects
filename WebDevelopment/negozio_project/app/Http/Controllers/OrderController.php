<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    //

    public function order($id) {

        if(Auth()->user()) {

            $current_time =  Carbon::now(new DateTimeZone('CET'));

        $product = Product::where('id_product', $id)->first();
        $shop = Shop::where('id_shop', $product->shop_id)->first();

        $amopening = Carbon::createFromFormat('H:i', $shop->amopening, 'CET');
        $amclosing = Carbon::createFromFormat('H:i', $shop->amclosing, 'CET');
        $pmopening = Carbon::createFromFormat('H:i', $shop->pmopening, 'CET');
        $pmclosing = Carbon::createFromFormat('H:i', $shop->pmclosing, 'CET');

        if (($current_time->gt($amopening) && $current_time->lessThan($amclosing)) || ($current_time->gt($pmopening) && $current_time->lessThan($pmclosing))) {

            return view('Order.createorder', ["id" => $id]);
        }
        return redirect("/");

        }else {
            return redirect("/login");
        }
        
        
    }

    public function createorder()
    {
        

        $current_time =  Carbon::now(new DateTimeZone('CET'));

        var_dump($current_time);

        $product = Product::where('id_product', request('product'))->first();
        $shop = Shop::where('id_shop', $product->shop_id)->first();

        $amopening = Carbon::createFromFormat('H:i', $shop->amopening, 'CET');
        $amclosing = Carbon::createFromFormat('H:i', $shop->amclosing, 'CET');
        $pmopening = Carbon::createFromFormat('H:i', $shop->pmopening, 'CET');
        $pmclosing = Carbon::createFromFormat('H:i', $shop->pmclosing, 'CET');
        var_dump($amopening);


        //var_dump( $current_time->gt($amopening) && $current_time->lessThan($amclosing) || ($current_time->gt($pmopening) && $current_time->lessThan($pmclosing)));

        
        if (($current_time->gt($amopening) && $current_time->lessThan($amclosing)) || ($current_time->gt($pmopening) && $current_time->lessThan($pmclosing))) {

            $order = new Order();
            $order->customer = request('customer');
            $order->product_id = $product->id_product;
            $order->save();
            return redirect("/");
        }
    }
}
