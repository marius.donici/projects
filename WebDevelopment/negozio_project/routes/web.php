<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', [ProductController::class, 'welcome']);
Route::get('/', [ProductController::class, 'filter']);
Route::get('/users', [UserController::class, 'users']);
Route::get('/users/{id}', [UserController::class, 'shops']);
Route::get('/register', [UserController::class, 'register']);
//Route::post('/register', [UserController::class, 'store']);
//Route::post('/login', [UserController::class, 'login']);
Route::get('/login', [UserController::class, 'login']);
Route::get('/shops', [ShopController::class, 'shops']);
Route::get('/shops/{id}', [ShopController::class, 'products']);
Route::get('/createorder/{id}', [OrderController::class, 'order']);
Route::post('/createorder/{id}', [OrderController::class, 'createorder']);




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/homeshop/{id}', [ShopController::class, 'homeshop']) -> middleware('auth');
Route::post('/homeshop/{id}', [ShopController::class, 'addproduct']) -> middleware('auth');
Route::get('/createshop', [ShopController::class, 'createshop']) -> middleware('auth');
Route::post('/createshop', [ShopController::class, 'saveshop']) -> middleware('auth');

