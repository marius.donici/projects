<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id_product');
            $table->string('name');
            $table->string('description');
            $table->integer('price');
            $table->integer('category_id')->unsigned(); 
            $table->integer('color_id')->unsigned();
            $table->integer('material_id')->unsigned();
            $table->integer('shop_id')->unsigned();
            $table->foreign('category_id')->references('id_category')->on('categories'); 
            $table->foreign('color_id')->references('id_color')->on('colors');
            $table->foreign('material_id')->references('id_material')->on('materials');
            $table->foreign('shop_id')->references('id_shop')->on('shops');  
            $table->timestamps();
            $table->index('id_product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
