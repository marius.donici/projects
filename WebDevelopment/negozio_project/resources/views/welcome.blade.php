@extends('layouts.app')

@section('content')
<div>
    <div>
        <form action="" method="get">
            @csrf
            <div>
                <h2>Filtri di ricerca</h2>
                <h3>Categoria</h3>
                <input type="radio" id="clothing" name="category" value="1">
                <label for="clothing">Abbigliamento</label><br>
                <input type="radio" id="car" name="category" value="2">
                <label for="car">Auto</label><br>
                <input type="radio" id="other" name="category" value="3">
                <label for="other">Altro</label>
            </div>
            <div>
                <h3>Colore</h3>
                <input type="radio" id="black" name="color" value="1">
                <label for="black">Nero</label><br>
                <input type="radio" id="red" name="color" value="3">
                <label for="red">Rosso</label><br>
                <input type="radio" id="green" name="color" value="4">
                <label for="green">Verde</label>
            </div>
            <div>
                <h3>Materiale</h3>
                <input type="radio" id="leather" name="material" value="1">
                <label for="leather">Pelle</label><br>
                <input type="radio" id="cloth" name="material" value="2">
                <label for="cloth">Stoffa</label><br>
                <input type="radio" id="mix" name="material" value="3">
                <label for="mix">Mix</label>
            </div>
            <input type="submit" value="Cerca prodotto">
    </div>
    </form>
</div>
<h1>Tutti i prodotti</h1>
<div>
    <table>
        <th>
            <tr>
                <td>Nome</td>
                <td>Descrizione</td>
                <td>Categoria</td>
                <td>Materiale</td>
                <td>Colore</td>
                <td>Negozio</td>
                <td>Prezzo</td>
            </tr>
        </th>
        <tbody>
            @foreach($products as $product)
            <tr>
                <td>{{$product->name}}</td>
                <td>{{$product->description}}</td>
                <td>{{$product->categories->category}} </td>
                <td>{{$product->materials->material}}</td>
                <td>{{$product->colors->color}}</td>
                <td><a href="/shops/{{$product->shop_id}}">{{$product->shops->name}}</a></td>
                <td>{{$product->price}}</td>
                <td>
                    @php
                    if (App\Http\Controllers\ShopController::is_open($product->shops->id_shop) == 1) {
                        echo 'Aperto';
                    } else {
                        echo 'Chiuso';
                    }
                    @endphp
                </td>
                <td><a href="/createorder/{{$product->id_product}}">Ordina prodotto</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
@endsection