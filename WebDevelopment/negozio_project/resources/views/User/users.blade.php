@extends('layouts.app')
@section('users')

<div>
    <h1>Utenti</h1>
    <table>
        <th>
            <tr>
                <td>Nome</td>
                <td>Email</td>

            </tr>
        </th>
        <tbody>

            @foreach($users as $user)
            <tr>
                <td>
                    {{$user->name}}
                </td>
                <td>{{$user->email}}</td>
                <td><a href="/users/{{$user->id_user}}">Visualizza i negozi</a></td>
            </tr>


            @endforeach
        </tbody>
    </table>
</div>
@endsection