<html>
<head>
<link rel="stylesheet" href="/css/main.css">
</head>
<body>
<ul>
  <li><a href="/">Home</a></li>
  <li><a href="/users">Utenti</a></li>
  <li><a href="/shops">Negozi</a></li>
  <li><input type="text" name="search" id="search"><input type="button" value="Cerca"></li>
  <li><a href="/login">Login</a></li>
  <li><a href="/register">Register</a></li>
</ul> 
@yield('content')
@yield('login')
@yield('auth.register')
@yield('users')
@yield('shops')
    
</body>
</html>