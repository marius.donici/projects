@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
            @if(Auth()->user()->admin_flag)
            <div>
                <a href="/createshop">Aggiungi negozio</a>
            </div>
            <div>
                <h3>I tuoi negozi</h3>
                <table>
                    <th>
                        <tr>
                            <td>Nome</td>

                        </tr>
                    </th>
                    <tbody>

                        @foreach($shops as $shop)
                        <tr>
                            <td>
                                <a href="/homeshop/{{$shop->id_shop}}"> {{$shop->name}}</a>
                            </td>
                        </tr>


                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif

            <div>
                <h3>Ordini</h3>
                <table>
                    <th>
                        <tr>
                            <td>Cliente</td>
                            <td>Prodotto</td>
                            <td>Negozio</td>

                        </tr>
                    </th>


                    <tbody>
                        @foreach($orders as $order)
                        <tr>
                            <td>{{$order->customer}}</td>
                            <td>{{$order->products->name}}</td>
                            <td><a href="/shops/{{$order->products->shops->id_shop}}"> {{$order->products->shops->name}}</a></td>
                

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
@endsection