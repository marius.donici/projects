@extends('layouts.app')

@section('content')
<div>
    <div>
        <table>
            <th>
                <tr>
                    <td>Nome</td>
                    <td>Descrizione</td>
                    <td>Categoria</td>
                    <td>Materiale</td>
                    <td>Colore</td>
                    <td>Negozio</td>
                    <td>Prezzo</td>
                </tr>
            </th>
            <tbody>
                @foreach($products as $product)
                <tr>
                    <td>{{$product->name}}</td>
                    <td>{{$product->description}}</td>
                    <td>{{$product->categories->category}} </td>
                    <td>{{$product->materials->material}}</td>
                    <td>{{$product->colors->color}}</td>
                    <td><a href="/shops/{{$product->shop_id}}">{{$product->shops->name}}</a></td>

                  
                    <td>{{$product->price}}</td>
                    <td><a href="/createorder/{{$product->id_product}}">Ordina prodotto</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection