@extends('layouts.app')
@section('shops')

<div>
    <h1>Negozi</h1>
    <table>
        <th>
            <tr>
                <td>Nome</td>

            </tr>
        </th>
        <tbody>

            @foreach($shops as $shop)
            <tr>
                <td>
                    {{$shop->name}}
                </td>
                <td><a href="/shops/{{$shop->id_shop}}">Visualizza i prodotti</a></td>
            </tr>


            @endforeach
        </tbody>
    </table>
</div>
@endsection