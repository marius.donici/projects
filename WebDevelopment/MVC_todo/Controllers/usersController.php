<?php

class usersController extends Controller
{
    function index()
    {
        if (isset($_POST["login"])) {
            require (ROOT . 'Models/User.php');
            $user = new User();
            
            if ($user->showUser($_POST["user"], $_POST["password"])) {
                session_start();
                $_SESSION["logged"] = "yes";
                $id = $user->showUser($_POST["user"], $_POST["password"]);
                $_SESSION["user"] = $id['id'];
                
                
                header("Location: " . WEBROOT . "tasks/index");
            }
        }
        $this->render("index");
    }



    function logout() {
        session_start();
        unset($_SESSION["user"]);
        unset($_SESSION["logged"]);
        session_destroy();
        header("Location: " . WEBROOT . "users/index");
    }

    function create()
    {
        if (isset($_POST["user"]) && isset($_POST["password"]) && isset($_POST["passwd"]) && isset($_POST["register"]))
        {
            require(ROOT . 'Models/User.php');

            $user= new User();

            if ($user->create($_POST["user"], $_POST["password"], $_POST["passwd"]))
            {
                session_start();
                $_SESSION["logged"] = "yes";
                $_SESSION["user"] = $user->getUser($_POST["user"])["id"];
                header("Location: " . WEBROOT . "tasks/index");
            }
        }

        $this->render("create");
    }

    /*function edit($id)
    {
        require(ROOT . 'Models/Task.php');
        $task= new Task();

        $d["task"] = $task->showTask($id);

        if (isset($_POST["title"]))
        {
            if ($task->edit($id, $_POST["title"], $_POST["description"]))
            {
                header("Location: " . WEBROOT . "tasks/index");
            }
        }
        $this->set($d);
        $this->render("edit");
    }*/

    /*function delete($id)
    {
        require(ROOT . 'Models/Task.php');

        $task = new Task();
        if ($task->delete($id))
        {
            header("Location: " . WEBROOT . "tasks/index");
        }
    }*/
}
?>