<?php
class taggedtasksController extends Controller
{
    function index()
    {
        require(ROOT . 'Models/TaggedTask.php');

        $tags = new Tag();

        $d['taggedtasks'] = $taggedtasks->showAllTaggedTasks();
        $this->set($d);
        $this->render("index");
    }

    function create()
    {
        if (isset($_POST["tag"]))
        {
            require(ROOT . 'Models/TaggedTask.php');

            $tag= new Tag();

            if ($tag->create($_POST["tag"]))
            {
                header("Location: " . WEBROOT . "taggedtasks/index");
            }
        }

        $this->render("create");
    }



    function delete($id)
    {
        require(ROOT . 'Models/TaggedTask.php');

        $tag= new TaggedTask();
        if ($tag->delete($id))
        {
            header("Location: " . WEBROOT . "tasks/index");
        }
    }
}
?>