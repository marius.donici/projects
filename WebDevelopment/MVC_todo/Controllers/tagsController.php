<?php
session_start();
class tagsController extends Controller
{
    function index()
    {
        require(ROOT . 'Models/Tag.php');

        $tags = new Tag();

        $d['tags'] = $tags->showAllTags();
        $this->set($d);
        $this->render("index");
    }

    function create()
    {
        
        if (isset($_POST["tag"]))
        {
            $url = $_SERVER["REQUEST_URI"];
            $explode_url = explode('/', $url);
            $explode_url = array_slice($explode_url, 2);//2

            $params= array_slice($explode_url, 2);//2
            require(ROOT . 'Models/Tag.php');
            require(ROOT. 'Models/TaggedTask.php');

            $tag= new Tag();

            if ($tag->create($_POST["tag"]))
            {
                $ts = new TaggedTask();
                $boh = $_POST['tag'];
                $i = $tag->getId($boh);
                if ($ts->create($params[0], $i['id'])) {
                    header("Location: " . WEBROOT . "tasks/index");
                }
            }
        }

        $this->render("create");
    }

    function edit($id)
    {
        require(ROOT . 'Models/Tag.php');
        $tag= new Tag();

        $d["tag"] = $tag->showTag($id);

        if (isset($_POST["title"]))
        {
            if ($tag->edit($id, $_POST["title"]))
            {
                header("Location: " . WEBROOT . "tasks/index");
            }
        }
        $this->set($d);
        $this->render("edit");
    }



    function delete($id)
    {
        require(ROOT . 'Models/Tag.php');

        $tag= new Tag();
        if ($tag->delete($id))
        {
            header("Location: " . WEBROOT . "tasks/index");
        }
    }
}
