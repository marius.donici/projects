<?php

class Router
{

    static public function parse($url, $request)
    {
        $url = trim($url);

        if ($url == "/MVC_todo/")
        {
            
            if (isset($_SESSION["logged"])) {
                $request->controller = "tasks";
            $request->action = "index";
            $request->params = [];
            }else {
                $request->controller = "users";
                $request->action = "index";
                $request->params = [];
            }
            
        }
        else
        {
            $explode_url = explode('/', $url);
            $explode_url = array_slice($explode_url, 2);//2
            $request->controller = $explode_url[0];//0
            $request->action = $explode_url[1];//1
            $request->params = array_slice($explode_url, 2);//2
        }

    }
}
?>