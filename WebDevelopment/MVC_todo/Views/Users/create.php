<form method="post" action="#">
  <!-- Email input -->
  <div class="form-outline mb-4">
    <input type="text" name="user" class="form-control" />
    <label class="form-label" for="user">Username</label>
  </div>

  <!-- Password input -->
  <div class="form-outline mb-4">
    <input type="password" name="password" class="form-control" />
    <label class="form-label" for="password">Password</label>
  </div>

  <div class="form-outline mb-4">
    <input type="password" name="passwd" class="form-control" />
    <label class="form-label" for="passwd">Reinsert password</label>
  </div>




  <!-- Submit button -->
  <button type="submit" name="register" class="btn btn-primary btn-block">Register</button>
</form>

<div class="form-outline mb-4">
    <a href="/MVC_todo/users/index/">I have already an account</a>
  </div>