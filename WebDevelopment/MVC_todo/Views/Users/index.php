
<form method="post" action="">
  <!-- Email input -->
  <div class="form-outline mb-4">
    <input type="text" name="user" class="form-control" />
    <label class="form-label" for="user">Username</label>
  </div>

  <!-- Password input -->
  <div class="form-outline mb-4">
    <input type="password" name="password" class="form-control" />
    <label class="form-label" for="password">Password</label>
  </div>



  <!-- Submit button -->
  <button type="submit" name="login" class="btn btn-primary btn-block">Login</button>

</form>

<div class="form-outline mb-4">
    <a href="/MVC_todo/users/create/">Create an account</a>
  </div>