<h1>Tag</h1>
<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>
        <a href="/MVC_todo/tags/create/" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new task</a>
            <th>Subtask</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <?php
        foreach ($tags as $tag)
        {
            echo '<tr>';
            echo "<td>" . $tag['tag'] . "</td>";
            echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/MVC_todo/tags/edit/" . $tag["id"] . "' ><span class='glyphicon glyphicon-edit'></span> Edit</a> <a href='/MVC_todo/tags/delete/" . $tag["id"] . "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";
            echo "</tr>";
        }
        ?>
    </table>
</div>