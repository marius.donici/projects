
<h1>Tasks</h1>
<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>
            <a href="/MVC_todo/tasks/create/" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new task</a>
            <tr>
                <th>ID</th>
                <th>Task</th>
                <th>Description</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>


            <?php
            
            foreach ($tasks as $task) {
                echo '<tr>';
                echo "<td>" . $task['id'] . "</td>";

                echo "<td>" . $task['title'] . "</td>";
                echo "<td>" . $task['description'] . "</td>";
                echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/MVC_todo/tasks/edit/" . $task["id"] . "' ><span class='glyphicon glyphicon-edit'></span> Edit</a> <a href='/MVC_todo/tasks/delete/" . $task["id"] . "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";
                echo "<td>";
                echo "</tr>";
                echo "<tr>";
                echo '<td><a href="/MVC_todo/tags/create/'.$task['id'].'" class="btn btn-secondary btn-xs pull-right"><b>+</b> Add new subtask</a></td>';
                //echo '<td><div id="flip">Visualizza i subtasks</div></td>';
                echo "</tr>";



                require_once(ROOT . 'Models/TaggedTask.php');
                require_once(ROOT . 'Models/Tag.php');

                $taggedtask = new TaggedTask();
                $taggedtasks = $taggedtask->showAllTaggedTasks();

                foreach ($taggedtasks as $tt) {

                    if ($tt['taskID'] == $task['id']) {
                        echo "<tr>";
                        $tag = new Tag();
                        $t = $tag->showTag($tt['tagID']);

                        echo "<td>" . $t['tag'] . "</td>";
                        echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/MVC_todo/tags/edit/" . $t["id"] . "' ><span class='glyphicon glyphicon-edit'></span> Edit</a> <a href='/MVC_todo/taggedtasks/delete/" . $tt["id"] . "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";

                        echo "</tr>";
                    }
                }
            }
            ?>

        </tbody>
    </table>



</div>