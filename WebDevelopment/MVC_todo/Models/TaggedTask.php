<?php
class TaggedTask extends Model
{
    public function create($task, $tag)
    {
        $sql = "INSERT INTO taggedtasks (taskID, tagID) VALUES (:task , :tag)";

        $req = Database::getBdd()->prepare($sql);


        return $req->execute([
            'task' => $task,
            'tag' => $tag,

        ]);
    }

    public function showTag($id)
    {
        $sql = "SELECT * FROM taggedtasks WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetch();
    }

    public function showAllTaggedTasks()
    {
        $sql = "SELECT * FROM taggedtasks";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

   /* public function edit($id, $tag)
    {
        $sql = "UPDATE taggedtasks SET tag = :tag WHERE id = :id";

        $req = Database::getBdd()->prepare($sql);

        return $req->execute([
            'id' => $id,
            'tag' => $tag

        ]);
    }*/

    public function delete($id)
    {
        $sql = 'DELETE FROM taggedtasks WHERE id = ?';
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([$id]);
    }
}
?>