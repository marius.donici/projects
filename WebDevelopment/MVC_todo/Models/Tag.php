<?php
class Tag extends Model
{
    public function create($tag)

    {
        $tag = strtoupper($tag);
        if ($this->getTag($tag) == NULL) {
            $sql = "INSERT INTO tags (tag) VALUES (:tag)";

            $req = Database::getBdd()->prepare($sql);

            return $req->execute([
                'tag' => $tag,


            ]);
        } else {
            return $this->getTag($tag);
        }
    }

    public function getId($tag)
    {
        $sql = "SELECT id FROM tags WHERE tag ='" . $tag . "' LIMIT 1";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $ret = $req->fetch();
        
        return $ret;
    }



    public function getTag($tag)
    {
        $sql = "SELECT * FROM tags WHERE tag ='" . $tag . "' LIMIT 1";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetch();
    }

    public function showTag($id)
    {
        $sql = "SELECT * FROM tags WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetch();
    }

    public function showAllTags()
    {
        $sql = "SELECT * FROM tags";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

    public function edit($id, $tag)
    {
        $sql = "UPDATE tags SET tag = :tag WHERE id = :id";

        $req = Database::getBdd()->prepare($sql);

        return $req->execute([
            'id' => $id,
            'tag' => $tag

        ]);
    }

    public function delete($id)
    {
        $sql = 'DELETE FROM tags WHERE id = ?';
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([$id]);
    }
}
